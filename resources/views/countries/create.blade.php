@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="country uper">
  <div class="country-header">
    Add Points Entry Item
  </div>
  <div class="country-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('countries.store') }}">
          @include('countries.form')
          <button type="submit" class="btn btn-primary">Add</button>
      </form>
  </div>
</div>
@endsection