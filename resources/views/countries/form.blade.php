<div class="form-group">
  @csrf
  <label for="name">Country Name:</label>
  <input type="text" class="form-control" name="country_name" value={{ $country->country_name }}></input>
</div>
<div class="form-group">
  <label for="name">Country Code:</label>
  <input type="text" class="form-control" name="country_code" value={{ $country->country_code }}></input>
</div>
<div class="form-group">
  <label for="name">Low Season:</label>
  <input type="text" class="form-control" name="low_season" value={{ $country->low_season }}></input>
</div>
<div class="form-group">
  <label for="name">Mid Season:</label>
  <input type="text" class="form-control" name="mid_season" value={{ $country->mid_season }}></input>
</div>
<div class="form-group">
  <label for="name">High Season:</label>
  <input type="text" class="form-control" name="high_season" value={{ $country->high_season }}></input>
</div>
<div class="form-group">
  <label for="name">Peak Season:</label>
  <input type="text" class="form-control" name="peak_season" value={{ $country->peak_season }}></input>
</div>