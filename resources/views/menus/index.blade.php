@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <a href="{{ route('menus.create')}}" class="btn btn-primary">Add</a>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Menu Text</td>
          <td colspan="3">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($menus as $menu)
        <tr>
            <td>{{$menu->id}}</td>
            <td>{{$menu->menu_text}}</td>
            <td><a href="{{ route('menus.edit',$menu->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('menus.destroy', $menu->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
            <td>
              <a href="{{ URL::to('/') }}/menu-items/{{$menu->id}}/create" class="btn btn-primary">Add Item</a>
              <a href="{{ URL::to('/') }}/menu-items/{{$menu->id}}" class="btn btn-primary">View Items</a>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection