@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <a href="{{ route('content-types.create')}}" class="btn btn-primary">Add</a>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Content Type Text</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($contentTypes as $contentType)
        <tr>
            <td>{{$contentType->id}}</td>
            <td>{{$contentType->content_type_text}}</td>
            <td><a href="{{ route('content-types.edit',$contentType->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('content-types.destroy', $contentType->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection