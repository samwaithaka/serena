<style>
  .left {
    text-align:left;
    wwidth:250px;
  }
  
  .right {
    text-align:right;
  }

  .underline {
    border-bottom: solid 2px #777;
  }

  .italics {
    font-style:italic;
  }

  .margin-20 {
    margin-top:20px;
  }

  .border {
    border:solid 1px #999;
  }
</style>
<div class="row margin-20">
  <div class="col-sm-12"> 
  <div class="col-sm-12 left">
    <h2>Find by country or hotel name</h2>
  </div>
    <form method="get" action="{{ URL::to('/') }}/prestige-card-hotels/show">
        <input  class="col-sm-6" type="text" class="form-control" name="searchKey"
        placeholder="Enter hotel name or country"></input>
        <button  class="col-sm-2" type="submit" class="btn btn-primary">Search</button>
    </form>
  </div>
</div>