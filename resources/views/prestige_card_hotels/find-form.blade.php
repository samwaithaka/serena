<style>
  .left {
    text-align:left;
    wwidth:250px;
  }
  
  .right {
    text-align:right;
  }

  .underline {
    border-bottom: solid 2px #777;
  }

  .italics {
    font-style:italic;
  }

  .margin-20 {
    margin-top:20px;
  }

  .border {
    border:solid 1px #999;
  }
</style>
<div class="row margin-20">
  <div class="col-sm-12"> 
  <div class="col-sm-12 left">
    <h2>Find by country or hotel name</h2>
  </div>
    <form method="get" action="{{ URL::to('/') }}/prestige-card-hotels/show">
        <select  class="col-sm-6" class="form-control" 
          onchange="this.form.submit()" name="country">
          <option value="">Select Country</option>
          @foreach ($countryList as $country)
            <option value="{{ $country }}"
             @if($country == $countryName) selected="selected" @endif
            >{{ $country }}</option>
          @endforeach
        </select>
        <select  class="col-sm-6" class="form-control" 
          onchange="this.form.submit()" name="hotel">
          <option value="">Select Hotel</option>
          @foreach ($hotelList as $hotel)
            <option value="{{ $hotel }}"
            @if($hotel == $hotelName) selected="selected" @endif
            >{{ $hotel }}</option>
          @endforeach
        </select>
    </form>
  </div>
</div>