@extends('layouts.spc-b.inner')

@section('caption')
Serena Prestige Points
@endsection

@section('filename')
background-image: url({{ asset('spc-b') }}/img/header3.jpg);
@endsection

@section('description')
Redeem your accumulated points for a variety of benefits that you deserve.
@endsection

@section('content')
<p>Experience world-class members only Prestige Club programme that grants you access to be part of the Serena loyalty family. As part of our family, experience uniquely personalized authentic travel experiences wherever you stay with us. Whether you are traveling for business or leisure, we are at your service to enhance your stay with our unique rewards that show you how valued you are.</p>
@include('prestige_card_hotels.find-form')
@endsection