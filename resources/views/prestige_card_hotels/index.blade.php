@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <a href="{{ route('prestige-card-hotels.create')}}" class="btn btn-primary">Add</a>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Hotel Name</td>
          <td colspan="5">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($prestigeCardHotels as $prestigeCardHotel)
        <tr>
            <td>{{$prestigeCardHotel->id}}</td>
            <td>{{$prestigeCardHotel->hotel_name}}</td>
            <td><a href="{{ route('prestige-card-hotels.edit',$prestigeCardHotel->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('prestige-card-hotels.destroy', $prestigeCardHotel->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
            <td><a href="{{ URL::to('/') }}/prestige-card-points/{{$prestigeCardHotel->id}}/create" class="btn btn-primary">Room Points</a>
            <a href="{{ URL::to('/') }}/prestige-card-points/{{$prestigeCardHotel->id}}" class="btn btn-primary">View</a>
            </td>

            <td><a href="{{ URL::to('/') }}/prestige-card-meal-points/{{$prestigeCardHotel->id}}/create" class="btn btn-primary">Meal Points</a>
            <a href="{{ URL::to('/') }}/prestige-card-meal-points/{{$prestigeCardHotel->id}}" class="btn btn-primary">View</a>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection