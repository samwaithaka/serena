@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit Prestige Card Hotel
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post"
      enctype="multipart/form-data"
      action="{{ route('prestige-card-hotels.update', $prestigeCardHotel->id) }}">
        @method('PATCH')
        @csrf
        @include('prestige_card_hotels.form')
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
  </div>
</div>
@endsection