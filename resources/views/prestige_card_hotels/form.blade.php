<div class="form-group">
  @csrf
  <label for="name">Hotel Name:</label>
  <input type="text" class="form-control" name="hotel_name" value={{ $prestigeCardHotel->hotel_name }}></input>
</div>
<div class="form-group">
  <label for="name">Remarks:</label>
  <input type="text" class="form-control" name="remarks" value={{ $prestigeCardHotel->remarks }}></input>
</div>
<div class="form-group">
  <label for="mode">Mode:</label>
  <input type="text" class="form-control" name="mode" value={{ $prestigeCardHotel->mode }}></input>
</div>
<div class="form-group">
  <label for="intro">Hotel Country:</label>
  <select class="form-control col-sm-4" name="country_id">
      @foreach($countries as $country)
      <option value="{{$country->id}}" @if($prestigeCardHotel->country_id === $country->id) selected @endif>
        {{ $country->country_name }}
      </option>
      @endforeach
  </select>
  <label for="imageInput">File input</label>
        <input data-preview="#preview" name="input_img" type="file" id="imageInput" />
</div>