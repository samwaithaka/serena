@extends('layouts.spc-b.inner')

@section('caption')
Serena Prestige Points
@endsection

@section('filename')
background-image: url({{ asset('spc-b') }}/img/header3.jpg);
@endsection

@section('description')
Redeem your accumulated points for a variety of benefits that you deserve.
@endsection

@section('content')
@include('prestige_card_hotels.find-form')
@if($hotelsSeasons[0]->low_season != "")
<table class="col-sm-8">
      <tr>
          <td class="border-right border-bottom left"><em>Low Season</em></td>
          <td class="left border-bottom">&nbsp;{{ $hotelsSeasons[0]->low_season }}</td>
          <td class="border-right border-bottom left"><em>Mid Season</em></td>
          <td class="left border-bottom">&nbsp;{{ $hotelsSeasons[0]->mid_season }}</td>
      </tr>
      <tr>
          <td class="border-right border-bottom left"><em>High Season</em></td>
          <td class="left border-bottom">&nbsp;{{ $hotelsSeasons[0]->high_season }}</td>
          <td class="border-right border-bottom left"><em>Peak Season</em></td>
          <td class="left border-bottom">&nbsp;{{ $hotelsSeasons[0]->peak_season }}</td>
      </tr>
     </table>
@endif
@foreach ($hotels as $hotel)
  <style>
    .border-right {
      border-right:solid 1px #ccc;
      padding-right:5px;
    }
  </style>
  <div class="col-sm-9 reward-points border">
    <?php if($hotel[4] == 1) { ?>
     <table>
        <tr><th class="left" colspan="6"><h4>{{ $hotel[0] }}, {{ $hotel[2] }}</h4></th></tr>
        <tr>
          <td class="border-right left underline italics">Room Type</td>
          <td class="border-right right underline italics">&nbsp;&nbsp;Low Season</td>
          <td class="border-right right underline italics">&nbsp;&nbsp;Mid Season</td>
          <td class="border-right right underline italics">&nbsp;&nbsp;High Season</td>
          <td class="border-right right underline italics">&nbsp;&nbsp;Peak Season</td>
        </tr>
        @foreach ($hotel[3] as $room)
        <tr>
          <td class="border-right left">{{ $room[6] }}</td>
          <td class="border-right right">{{ $room[0] }}</td>
          <td class="border-right right">{{ $room[1] }}</td>
          <td class="border-right right">{{ $room[2] }}</td>
          <td class="border-right right">{{ $room[3] }}</td>
        </tr>
        @endforeach
        <tr><td class="left border-top" colspan="6">{{ $hotel[1] }}</td></tr>
     </table>
     <?php } elseif ($hotel[4] == 2) { ?>
      <table>
        <tr><th class="left" colspan="6"><h4>{{ $hotel[0] }}, {{ $hotel[2] }}</h4></th></tr>
        <tr>
          <td class="border-right left underline italics">Room Type</td>
          <td class="border-right right underline italics">&nbsp;&nbsp;Low Season</td>
          <td class="border-right right underline italics">&nbsp;&nbsp;Mid Season</td>
          <td class="border-right right underline italics">&nbsp;&nbsp;High Season</td>
          <td class="border-right right underline italics">&nbsp;&nbsp;Peak Season</td>
          <td class="border-right right underline italics">&nbsp;&nbsp;Upgrade Points</td>
        </tr>
        @foreach ($hotel[3] as $room)
        <tr>
          <td class="border-right left">{{ $room[6] }}</td>
          <td class="border-right right">{{ $room[0] }}</td>
          <td class="border-right right">{{ $room[1] }}</td>
          <td class="border-right right">{{ $room[2] }}</td>
          <td class="border-right right">{{ $room[3] }}</td>
          <td class="border-right right">{{ $room[4] }}</td>
        </tr>
        @endforeach
        <tr><td class="left border-top" colspan="6">{{ $hotel[1] }}</td></tr>
     </table>
     <?php } elseif ($hotel[4] == 3) { ?>
      <table>
        <tr><th class="left" colspan="6"><h4>{{ $hotel[0] }}, {{ $hotel[2] }}</h4></th></tr>
        <tr>
          <td class="border-right left underline italics">Room Type</td>
          <td class="border-right right underline italics">&nbsp;&nbsp;Points</td>
        </tr>
        @foreach ($hotel[3] as $room)
        <tr>
          <td class="border-right left">{{ $room[6] }}</td>
          <td class="border-right right">{{ $room[5] }}</td>
        </tr>
        @endforeach
        <tr><td class="left border-top" colspan="6">{{ $hotel[1] }}</td></tr>
     </table>
     <?php } else { ?>
      <table>
        <tr><th class="left" colspan="6"><h4>{{ $hotel[0] }}, {{ $hotel[2] }}</h4></th></tr>
        <tr>
          <td class="border-right left underline italics">Room Type</td>
          <td class="border-right right underline italics">&nbsp;&nbsp;Points</td>
          <td class="border-right right underline italics">&nbsp;&nbsp;Upgrade Points</td>
        </tr>
        @foreach ($hotel[3] as $room)
        <tr>
          <td class="border-right left">{{ $room[6] }}</td>
          <td class="border-right right">{{ $room[5] }}</td>
          <td class="border-right right">{{ $room[4] }}</td>
        </tr>
        @endforeach
        <tr><td class="left border-top" colspan="6">{{ $hotel[1] }}</td></tr>
     </table>
     <?php } ?>
     </div>
     <div class="col-sm-3 reward-points">
     <?php if(!empty($hotel[5])) { ?>
     <table class="col-sm-12">
      <tr><td colspan="2">&nbsp;</td></tr>
      <tr><th colspan="2">Meals Redeem Points</th></tr>
      <tr>
          <td class="border-right border-bottom left">Meal *</td><td class="right border-bottom">&nbsp;Points</td>
      </tr>
      <tr>
          <td class="border-right left">Breakfast</td><td class="right">&nbsp;{{ $hotel[5][0] }}</td>
      </tr>
      <tr>
          <td class="border-right left">Lunch</td><td class="right">&nbsp;{{ $hotel[5][1] }}</td>
      </tr>
      <tr>
          <td class="border-right left">Dinner</td><td class="right">&nbsp;{{ $hotel[5][2] }}</td>
      </tr>
      <tr>
          <td colspan="2" class="border-top left">* Per Person</td>
      </tr>
     </table>
     <?php } ?>
  </div>
@endforeach
</div>
@endsection