<?php 
  use App\MenuItem;
  use App\Menu;
  //use Auth;
  $menuItem = substr($_SERVER['REQUEST_URI'], 7, strlen($_SERVER['REQUEST_URI']));
  $menuItems = DB::table('menu_items')
          ->join('menus','menu_items.menu_id','menus.id')
          ->where('menus.menu_text','Main Menu')
          ->get();
  $count = count($menuItems);
  $name = Auth::user();
  if(strlen($name['name']) > 0) {
        $name = $name['name'];
        $name = substr($name, 0, stripos($name, " "));
  }
?>

<ul class="probootstrap-main-nav col-sm-11">
    @for ($i = 0; $i < $count; $i++)
        <li 
          @if($menuItem == $menuItems[$i]->uri)
          class="active"
          @else 
          @endif
        >
          @if($menuItems[$i]->uri == 'https://www.serenahotels.com/en/experiences.html')
            <a target="_blank" href="{{ $menuItems[$i]->uri }}">{{ $menuItems[$i]->title }}<span class="menu-item-separator">|</span></a>
          @else
            <a href="{{ URL::to('/') }}/{{ $menuItems[$i]->uri }}">{{ $menuItems[$i]->title }}<span class="menu-item-separator">|</span></a>
          @endif
        @if($menuItems[$i]->title == "Membership")
          @include('dropdown')
        @endif
        </li>
    @endfor
    <li class="destinations">
      <a href="#">Destinations</a>
      <ul>
        @include('destination-dropdown')
      </ul>
    </li>
    @guest
      |&nbsp;&nbsp;<!--li><a href="{{ URL::to('/') }}/login"><i class="icon-user" aria-hidden="true">Sign In</i></a></li-->
      <li
          @if($menuItem == 'members/enrol')
          class="active"
          @else 
          @endif
      ><a href="{{ URL::to('/') }}/members/enrol"><i class="icon-users" aria-hidden="true">Enrol</i></a></li>
    @else
      |&nbsp;<li><i aria-hidden="true">Hello, {{ $name }}!</i></li>
      |&nbsp;<li><a href="{{ route('logout') }}"
        onclick="event.preventDefault();
             document.getElementById('logout-form').submit();"><i class="icon-log-out" aria-hidden="true">Sign Out</i></a></li>
    @endguest
    <li><a href="{{ URL::to('/') }}/member/points"><i class="icon-list" aria-hidden="true">Check Points</i></a></li>
    </ul>
    <ul class="col-sm-1">
    <li class="availability-button" style="list-style:none;">
      <!--a href="#" id="availability-button" class="btn btn-primary" style="width:120px;font-size: 1.0em;padding-top: 4px;">Availability</a-->
      <a href="#" id="availability-button" class=""><img style="margin-top:-20px;height:110px;" src="{{ asset('spc-b') }}/img/availability.png" /></a>
      <ul>
        @include('availability')
      </ul>
    </li>
</ul>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
</form>