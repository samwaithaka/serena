<style>
    .destinations-menu {
        background:#fff;
        text-align: left;
        font-size:75%;
        margin:50px auto;
        position:fixed;
        left:0px;
        height:100%;
        width: 100%;
    }

    .asia-region-map {
        background-image: url({{ asset('spc-b') }}/img/asia_bg.png);
        background-repeat: no-repeat;
    }

    .africa-region-map {
        background-image: url({{ asset('spc-b') }}/img/africa_bg.png);
        background-repeat: no-repeat;
        background-position: 170px 100px;
    }
</style>
<div class="container destinations-menu clearfix">
    <div class="row">
        <div class="col-sm-7 africa-region-map bg">
            <div class="col-sm-2">
                <h3>Africa</h3>
            </div>
            <div class="col-sm-10">
                <div class="col-sm-6">
                    <div>
                        <h4>KENYA</h4>
            <a href="https://www.serenahotels.com/serenanairobi/default-en.html" target="_blank">NAIROBI SERENA HOTEL</a><br />
            <a href="https://www.serenahotels.com/serenabeachhotel/default-en.html" target="_blank">Serena Beach Resort &amp; SPA</a><br />
            <a href="https://www.serenahotels.com/serenasweetwaters/default-en.html" target="_blank">SWEETWATERS SERENA CAMP</a><br />
            <a href="https://www.serenahotels.com/serenaelmenteita/default-en.html" target="_blank">LAKE ELMENTEITA SERENA CAMP</a><br />
            <a href="https://www.serenahotels.com/serenamara/default-en.html" target="_blank">MARA SERENA SAFARI LODGE</a><br />
            <a href="https://www.serenahotels.com/serenaamboseli/default-en.html" target="_blank">AMBOSELI SERENA SAFARI LODGE</a><br />
            <a href="https://www.serenahotels.com/serenakilaguni/default-en.html" target="_blank">KILAGUNI SERENA SAFARI LODGE</a><br />
            <a href="https://www.serenahotels.com/serenamountainlodge/default-en.html" target="_blank">SERENA MOUNTAIN LODGE</a><br />
            <a href="https://www.serenahotels.com/serenaolpejeta/default-en.html" target="_blank">OL PEJETA HOUSE</a>
                    </div>
                    <div>
                        <h4>RWANDA</h4>
            <a href="https://www.serenahotels.com/serenakigali/default-en.html" target="_blank">KIGALI SERENA HOTEL</a><br />
            <a href="https://www.serenahotels.com/serenalakekivu/default-en.html" target="_blank">LAKE KIVU SERENA HOTEL</a>
                    </div>
                    <div>
                        <h4>UGANDA</h4>
            <a href="https://www.serenahotels.com/serenakampala/default-en.html" target="_blank">KAMPALA SERENA HOTEL</a><br />
            <a href="https://www.serenahotels.com/serenalakevictoria/default-en.html" target="_blank">LAKE VICTORIA SERENA GOLF RESORT &amp; SPA</a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div>
                        <h4>TANZANIA</h4>
            <a href="https://www.serenahotels.com/serenadaressalaam/default-en.html" target="_blank">DAR ES SALAAM SERENA HOTEL</a><br />
            <a href="https://www.serenahotels.com/serenazanzibar/default-en.html" target="_blank">ZANZIBAR SERENA HOTEL</a><br />
            <a href="https://www.serenahotels.com/serenaarusha/default-en.html" target="_blank">ARUSHA SERENA HOTEL, RESORT &amp; SPA</a><br />
            <a href="https://www.serenahotels.com/serenambuzi/default-en.html" target="_blank">MBUZI MAWE SERENA CAMP</a><br />
            <a href="https://www.serenahotels.com/serenakirawira/default-en.html" target="_blank">KIRAWIRA SERENA CAMP</a><br />
            <a href="https://www.serenahotels.com/serenaselous/default-en.html" target="_blank">SELOUS SERENA CAMP</a><br />
            <a href="https://www.serenahotels.com/serenangorongoro/default-en.html" target="_blank">NGORONGORO SERENA SAFARI LODGE</a><br />
            <a href="https://www.serenahotels.com/serenaserengeti/default-en.html" target="_blank">SERENGETI SERENA SAFARI LODGE</a><br />
            <a href="https://www.serenahotels.com/serenalakemanyara/default-en.html" target="_blank">LAKE MANYARA SERENA SAFARI LODGE</a><br />
            <a href="https://www.serenahotels.com/serenamivumo/default-en.html" target="_blank">SERENA MIVUMO RIVER LODGE</a>
                    </div>
                    <div>
                        <h4>MOZAMBIQUE</h4>
            <a href="https://www.serenahotels.com/serenapolana/default-en.html" target="_blank">POLANA SERENA HOTEL</a>
                    </div>
                    <div>
                        <h4>DEMOCRATIC REPUBLIC OF CONGO (DRC)</h4>
            <a href="https://www.serenahotels.com/gomaserena/default-en.html" target="_blank">GOMA SERENA HOTEL, NORTH KIVU</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-5 asia-region-map bg">
            <div class="col-sm-2 align-bottom">
                <h3>Asia</h3>
            </div>
            <div class="col-sm-10">
                <div>
                    <div>
                        <h4>AFGHANISTAN</h4>
          <a href="https://www.serenahotels.com/serenakabul/default-en.html" target="_blank">KABUL SERENA HOTEL</a>
                    </div>
                    <div>
                        <h4>PAKISTAN</h4>
          <a href="https://www.serenahotels.com/serenaislamabad/default-en.html" target="_blank">ISLAMABAD SERENA HOTEL</a><br />
          <a href="https://www.serenahotels.com/serenafaisalabad/default-en.html" target="_blank">FAISALABAD SERENA HOTEL</a><br />
          <a href="https://www.serenahotels.com/serenaquetta/default-en.html" target="_blank">QUETTA SERENA HOTEL</a><br />
          <a href="https://www.serenahotels.com/serenaswat/default-en.html" target="_blank">SWAT SERENA HOTEL</a><br />
          <a href="https://www.serenahotels.com/serenagilgit/default-en.html" target="_blank">GILGIT SERENA HOTEL</a><br />
          <a href="https://www.serenahotels.com/serenahunza/default-en.html" target="_blank">HUNZA SERENA INN</a><br />
          <a href="https://www.serenahotels.com/serenashigarf/default-en.html" target="_blank">SERENA SHIGAR FORT (HERITAGE HOTEL)&nbsp;</a><br />
          <a href="https://www.serenahotels.com/serenakhaplupalace/default-en.html" target="_blank">SERENA KHAPLU PALACE (HERITAGE HOTEL)</a>
                    </div>
                    <div>
                        <h4>TAJIKISTAN</h4>
          <a href="https://www.serenahotels.com/serenadushanbe/default-en.html" target="_blank">Dushanbe Serena Hotel</a><br />
          <a href="https://www.serenahotels.com/serenakhorog/default-en.html" target="_blank">Khorog Serena Inn</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>