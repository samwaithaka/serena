@extends('layouts.spc-b.inner')

@section('title')
{{ $page->title }}
@endsection

@section('caption')
@if($page->slug == "faq" || $page->slug == "privacy-policy" || $page->slug == "terms-conditions")
{{ $page->caption }}
@else
@endif
@endsection

@section('filename')
background-image: url({{ asset('uploads/' . $page->filename) }});
@endsection

@section('description')
@if($page->slug == "faq" || $page->slug == "privacy-policy" || $page->slug == "terms-conditions")
{{ $page->description }}
@else
@endif
@endsection

@section('content')
<h2>{{ $page->title }}</h2>
{!! $page->body !!}
@endsection