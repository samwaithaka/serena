<style>
    .dropdown {
        background:#fff;
        text-align: left;
        font-size:75%;
        margin:50px auto;
        position:relative;
        left:0px;
        height:50px;
        width: 100%;
    }

    ul.active ul.dropdown li {
        border-bottom: none !important;
    }
</style>

<ul class="dropdown">
    <li><a href="{{ URL::to('/') }}/en/classic">Classic</a></li><span class="menu-item-separator">|</span>
    <li><a href="{{ URL::to('/') }}/en/gold">Gold</a></li><span class="menu-item-separator">|</span>
    <li><a href="{{ URL::to('/') }}/en/platinum">Platinum</a></li>
</ul>