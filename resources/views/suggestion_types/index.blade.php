@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <a href="{{ route('suggestion-types.create')}}" class="btn btn-primary">Add</a>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Suggestion Type</td>
          <td colspan="3">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($suggestionTypes as $suggestionType)
        <tr>
            <td>{{$suggestionType->id}}</td>
            <td>{{$suggestionType->suggestion_type}}</td>
            <td><a href="{{ route('suggestion-types.edit',$suggestionType->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('suggestion-types.destroy', $suggestionType->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
            <td>
              <a href="{{ URL::to('/') }}/suggestions/{{$suggestionType->id}}/create" class="btn btn-primary">Add Item</a>
              <a href="{{ URL::to('/') }}/suggestions/{{$suggestionType->id}}" class="btn btn-primary">View Items</a>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection