<div class="form-group">
  @csrf
  <label for="name">Suggestion Type:</label>
  <input type="text" class="form-control" name="suggestion_type" value={{ $suggestionType->suggestion_type }}></input>
  <label for="name">Description:</label>
  <input type="text" class="form-control" name="description" value={{ $suggestionType->description }}></input>
</div>
