<div class="form-group">
  @csrf
  <label for="name">Title:</label>
  <input type="text" class="form-control" name="title" value={{ $content->title }}></input>
</div>
<div class="form-group">
  <label for="intro">Content Category :</label>
  <select class="form-control" name="content_type_id">
  	  @foreach($items as $item)
	    <option value="{{$item->id}}" @if($content->content_type_id === $item->id) selected @endif>
	    	{{$item->content_type_text}}
	    </option>
	  @endforeach
  </select>
</div>
<div class="form-group">
  <label for="description">Description :</label>
  <textarea class="form-control" name="description">{{ $content->description }}</textarea>
</div>
<div class="form-group">
  <label for="intro">Intro :</label>
  <textarea class="form-control" name="intro">{{ $content->intro }}</textarea>
</div>

<div class="form-group">
  <label for="caption">Caption:</label>
  <input type="text" class="form-control" name="caption" value={{ $content->caption }}></input>
</div>

<div class="form-group">
  <label for="body">Body:</label>
  <textarea class="form-control" id="body" name="body">{{ $content->body }}</textarea>
</div>
<div class="form-group">
  <label for="filename">Header Image:</label>
  <input data-preview="#preview" name="filename" type="file" id="filename" />
</div>