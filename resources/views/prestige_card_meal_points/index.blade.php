@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <a href="{{$prestigeCardMealPoints[0]->hotel_id}}/create" class="btn btn-primary">Add Item</a>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Breakfast</td>
          <td>Lunch</td>
          <td>Dinner</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($prestigeCardMealPoints as $prestigeCardMealPoint)
        <tr>
            <td>{{$prestigeCardMealPoint->id}}</td>
            <td>{{$prestigeCardMealPoint->breakfast_points}}</td>
            <td>{{$prestigeCardMealPoint->lunch_points}}</td>
            <td>{{$prestigeCardMealPoint->dinner_points}}</td>
            <td><a href="{{ route('prestige-card-meal-points.edit',$prestigeCardMealPoint->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('prestige-card-meal-points.destroy', $prestigeCardMealPoint->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection