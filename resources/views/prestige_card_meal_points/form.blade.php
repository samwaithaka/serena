<div class="form-group">
  @csrf
  <label for="name">Breakfast:</label>
  <input type="text" class="form-control" name="breakfast_points" value={{ $prestigeCardMealPoint->breakfast_points }}></input>
</div>
<div class="form-group">
  <label for="name">Lunch:</label>
  <input type="text" class="form-control" name="lunch_points" value={{ $prestigeCardMealPoint->lunch_points }}></input>
</div>
<div class="form-group">
  <label for="name">Dinner:</label>
  <input type="text" class="form-control" name="dinner_points" value={{ $prestigeCardMealPoint->dinner_points }}></input>
</div>
<input type="hidden" name="hotel_id" value={{ $prestigeCardMealPoint->hotel_id }}></input>