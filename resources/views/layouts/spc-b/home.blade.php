<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Serena Prestige Club</title>
    <meta name="description" content="Serena Prestige Club">
    <meta name="keywords" content="Serena Prestige Club">
    
    <!-- Favicon -->
    <link rel="icon" href="{{ asset('spc-b') }}/img/favicon.ico" />

    <link href="https://fonts.googleapis.com/css?family=Crimson+Text:300,400,700|Rubik:300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('spc-b/css/styles-merged.css') }}">
    <link rel="stylesheet" href="{{ asset('spc-b/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('spc-b/css/custom.css') }}">

    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.min.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.min.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
   <script>
   $(document).ready(function(){
     var date_input=$('input[name="date_of_birth"]'); //our date input has the name "date"
     var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
     var options={
       format: 'yyyy-mm-dd',
       container: container,
       todayHighlight: true,
       autoclose: true,
     };
     date_input.datepicker(options);
     $('.availability-form').css('visibility','hidden');
     $('#availability-button').click(function(){
          if($('.availability-form').css('visibility') == 'hidden') {
            $('.availability-form').css('visibility','visible');
          } else {
            $('.availability-form').css('visibility','hidden');
          }
      });
      $('#email-address').click(function(){
         $('#email-address').val('');
      });
      $('.subscribe-dropdown').click(function(){
         $('#subscribe-btn-lst').addClass("active");
      });

      $('#subscribe-btn-africa').click(function(){
         var button = $("#subscribe-btn-africa");
         var link = button.data("newslatterurl");
         var email = $("#email-address").val();
         if(email != "") {
             window.location.href = link + email;
         }
      });

      $('#subscribe-btn-asia').click(function(){
         var button = $("#subscribe-btn-asia");
         var link = button.data("newslatterurl");
         var email = $("#email-address").val();
         if(email != "") {
             window.location.href = link + email;
         }
      });
   });
   </script>
  </head>
  <body>

  <!-- START: header -->
  <header role="banner" class="probootstrap-header">
    <!-- <div class="container"> -->
    <div class="row">
        <a href="/en/home" class="probootstrap-logo visible-xs-tmp"><img src="{{ asset('spc-b') }}/img/logo.png" class="hires margin-left-15" height="60" alt="Serena Prestige Club"></a>
        
        <a href="#" class="probootstrap-burger-menu visible-xs"><i>Menu</i></a>
        <div class="mobile-menu-overlay"></div>

        <nav role="navigation" class="probootstrap-nav hidden-xs">  
          <!-- Insert menu here -->
          <div class="row align-middle">
          <div class="col-md-10" style="height:45px;">
          @include('menu')
          </div>
        </div>
          <div class="extra-text visible-xs">
            <a href="#" class="probootstrap-burger-menu"><i>Menu</i></a>
            <h5>Connect With Us</h5>
            <ul class="social-buttons">
              <li><a href="#"><i class="icon-twitter"></i></a></li>
              <li><a href="#"><i class="icon-facebook2"></i></a></li>
              <li><a href="#"><i class="icon-instagram2"></i></a></li>
            </ul>
          </div>
        </nav>
        </div>
    <!-- </div> -->
  </header>
  <!-- END: header -->

  <section class="probootstrap-slider flexslider">
    <ul class="slides">
       @include('suggestions.slide')
    </ul>
  </section>

  <section class="probootstrap-section">
    <div class="container">
      <div class="row mb30">
        <div class="col-md-8 col-md-offset-2 probootstrap-section-heading text-center">
          <h2>Serena Prestige Club</h2>
          <p class="lead">Exotic destinations| Authentic experiences| Exhilarating adventures| Memorable moments| discover the magic Serena Hotels has in store for you. Prestige Club is designed to inspire travelers to gain more value from their travel experiences. Members are treated as royalty and are recognized with special benefits for their patronage.
        </div>
      </div>
    </div>
  </section>
  <section class="probootstrap-section">
    <div class="container">
      <div class="row mb30">
        <div class="col-md-8 col-md-offset-2 probootstrap-section-heading text-center">
           @include('suggestions.offers')
         </div>
      </div>
    </div>
  </section>
<!-- START: footer -->
<footer role="contentinfo" class="probootstrap-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center"><img src="{{ asset('spc-b') }}/img/logo-white.png" class="" height="60" alt="Serena Prestige Club"></a></div>
        <div class="col-md-4">
          <div class="probootstrap-footer-widget">
            <h3>Contact - Africa</h3>
            <ul class="probootstrap-contact-info">
              <li><i class="icon-location2"></i> <span>George Williamson House 4th Floor, 4th Ngong Avenue P.O. BOX 48690 – 00100 Nairobi,<br /> Kenya</span></li>
              <li><i class="icon-mail"></i><span>prestigeclub@serena.co.ke</span></li>
              <li><i class="icon-phone2"></i><span>+254 732 123 333; +254 709 998 333; +254 20 2842 333</span></li>
            </ul>
            <div class="ftr-smo-bar">
              <span class="smo-icon bg youtube">
                <a href="https://www.youtube.com/channel/UCq56dnBSnKxeTpSDHE7fEiw" target="_blank">
                  <i class="icon-youtube"></i>
                </a>
              </span>
              <span class="smo-icon bg facebook">
                <a href="https://www.facebook.com/SerenaHotels/" target="_blank">
                  <i class="icon-facebook"></i>
                </a>
              </span>
              <span class="smo-icon bg twitter">
                <a href="https://twitter.com/serenahotels" target="_blank">
                  <i class="icon-twitter"></i>
                </a>
              </span>
              <span class="smo-icon bg instagram">
                <a href="https://www.instagram.com/serenahotels/" target="_blank">
                  <i class="icon-instagram"></i>
                </a>
              </span>
              <span class="smo-icon bg googleplus">
                <a href="https://plus.google.com/u/0/+serenahotels" target="_blank">
                  <i class="icon-googleplus"></i>
                </a>
              </span>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <h3 class="ftr-block-title">Subscribe To Our Newsletter</h3>
          <div class="subscribe-block">
          <input type="text" id="email-address" name="email" value="Email address">
          <span class="subscribe-dropdown bg" style="font-size: 2.0em;"><i class="icon-paper-plane"></i></span>
          <div id="subscribe-btn-lst" class="subscribe-btn-lst">
          <span id="subscribe-btn-asia" class="subscribe-btn asia" data-newslatterurl="https://www.serenahotels.com/en/newsletter.html#asia,">Asia<i class="icon-arrow-right"></i></span>
          <span id="subscribe-btn-africa" class="subscribe-btn africa" data-newslatterurl="https://www.serenahotels.com/en/newsletter.html#africa,">Africa<i class="icon-arrow-right"></i></span>
          </div>
          </div>
          <ul class="probootstrap-contact-info">
              <li><a style="color:#fff;" target="_blank" href="{{ URL::to('/') }}/en/terms-conditions-1">Terms & Conditions</a></li>
              <li><a style="color:#fff;" target="_blank" href="{{ URL::to('/') }}/en/privacy-policy">Privacy Policy</a></li>
              <li><a style="color:#fff;" target="_blank" href="{{ URL::to('/') }}/en/faq">FAQ</a></li>
          </ul>
        </div>
        
        <div class="col-md-4">
          <div class="probootstrap-footer-widget">
            <h3>Contact - Asia</h3>
            <ul class="probootstrap-contact-info">
              <li><i class="icon-location2"></i> <span>Islamabad Serena Hotel, Khayaban-e-Suharwardy, G-5/1
Islamabad, <br />Pakistan</span></li>
              <li><i class="icon-mail"></i><span>prestigeclub@serena.com.pk</span></li>
              <li><i class="icon-phone2"></i><span>T: +92 51 287 4000 UAN: +92 51 111 133 133 F: +92 51 287 1006</span></li>
            </ul>
            <div class="ftr-smo-bar">
              <span class="smo-icon bg facebook">
                <a href="https://www.facebook.com/SerenaHotelsOfficial/" target="_blank">
                  <i class="icon-facebook"></i>
                </a>
              </span>
              <span class="smo-icon bg twitter">
                <a href="https://twitter.com/serena_hotels" target="_blank">
                  <i class="icon-twitter"></i>
                </a>
              </span>
              <span class="smo-icon bg instagram">
                <a href="https://www.instagram.com/serenahotelsofficial/" target="_blank">
                  <i class="icon-instagram"></i>
                </a>
              </span>
            </div>
          </div>
        </div>

        

      </div>
      <div class="row mt40">
        <div class="col-md-12 text-center">
          <p></p>
          <p>
            <small>&copy; 2019 <a href="https://www.serenahotels.com/" target="_blank">Serena Hotels</a>. All Rights Reserved | <a href="https://www.serenaprestigeclub.com/en/terms-conditions" target="_blank">Terms & Conditions</a><br> Designed &amp; Developed by <a href="https://www.brighterdaymedia.com/" target="_blank">BrighterDayMedia Ltd</a></small>
          </p>
        </div>
      </div>
    </div>
  </footer>
  <!-- END: footer -->
  
  <script src="{{ asset('spc-b/js/scripts.min.js') }}"></script>
  <script src="{{ asset('spc-b/js/main.min.js') }}"></script>
  <script src="{{ asset('spc-b/js/custom.js') }}"></script>
  </body>
</html>