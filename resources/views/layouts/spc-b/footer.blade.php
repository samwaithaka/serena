<!-- START: footer -->
<footer role="contentinfo" class="probootstrap-footer">
  <div class="container border-top">
    <div class="row">
      <div class="col-md-4">
        <div class="probootstrap-footer-widget">
          <p class="mt40"><img src="{{ asset('spc-b') }}/img/serena-spa.jpg" class="hires" height="100" alt="Serena Prestige Club"></p>
          <p>Live a well-travelled life in style and experience authentic cuisines, indigenous aesthetics and spectacular delights in some of the world’s most extraordinary locations with Serena Hotels. Our diverse collection of thirty-five hotels, resorts, safari lodges, camps and forts within East Africa, Mozambique and South and Central Asia are for those who lean towards distinctive and inspirational experiences.</p>
          <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
        </div>
      </div>
      <div class="col-md-4">
        <div class="probootstrap-footer-widget">
          <h3>Destinations</h3>
          <ul class="probootstrap-blog-list">
            <li>
              <a href="#">
                <figure class="probootstrap-image"><img src="{{ asset('spc-b') }}/img/nairobi-serena-hotel.jpg" alt="Serena Prestige Club" class="img-responsive"></figure>
                <div class="probootstrap-text">
                  <h4>Nairobi Serena Hotel</h4>
                  <span class="meta">Where to stay in Nairobi</span>
                  <p>Nairobi Serena Hotel enjoys a coveted location in the city centre, adjacent to Central Park</p>
                </div>
              </a>
            </li>
            <li>
              <a href="#">
                <figure class="probootstrap-image"><img src="{{ asset('spc-b') }}/img/serena-beach.jpg" alt="Serena Prestige Club" class="img-responsive"></figure>
                <div class="probootstrap-text">
                  <h4>Serena Beach Resort & Spa</h4>
                  <span class="meta">5 Star Mombasa Beach Resort</span>
                  <p>Guests arriving at Serena Beach Resort & Spa find an oasis of tranquillity framed by whispering coconut palms, lush gardens...</p>
                </div>
              </a>
            </li>
            <li>
              <a href="#">
                <figure class="probootstrap-image"><img src="{{ asset('spc-b') }}/img/ole-pejeta.jpg" alt="Serena Prestige Club" class="img-responsive"></figure>
                <div class="probootstrap-text">
                  <h4>Sweetwaters Serena Camp</h4>
                  <span class="meta">Ol Pejeta Conservancy Tented Safari Camp</span>
                  <p>Embrace the awe-inspiring natural beauty of your surroundings, and set the stage for a visit you won’t soon forget.</p>
                </div>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-4">
        <div class="probootstrap-footer-widget">
          <h3>Contact</h3>
          <ul class="probootstrap-contact-info">
            <li><i class="icon-location2"></i> <span>George Williamson House 4th Floor, 4th Ngong Avenue P.O. BOX 48690 – 00100 Nairobi, Kenya</span></li>
            <li><i class="icon-mail"></i><span>info@serenaprestigeclub.com</span></li>
            <li><i class="icon-phone2"></i><span>+254 732 123 333; +254 709 998 333; +254 20 2842 333</span></li>
          </ul>
          
        </div>
      </div>
    </div>
    <div class="row mt40">
      <div class="col-md-12 text-center">
        <ul class="probootstrap-footer-social">
          <li><a href=""><i class="icon-twitter"></i></a></li>
          <li><a href=""><i class="icon-facebook"></i></a></li>
          <li><a href=""><i class="icon-instagram2"></i></a></li>
        </ul>
        <p>
          <small>&copy; 2019 <a href="https://www.serenahotels.com/" target="_blank">Tourism Promotion Services Kenya</a>. All Rights Reserved. <br> Designed &amp; Developed by <a href="https://www.innovationslate.com/" target="_blank">Innovation Slate Ltd</a></small>
        </p>
        
      </div>
    </div>
  </div>
</footer>
<!-- END: footer -->

<script src="{{ asset('spc-b/js/scripts.min.js') }}"></script>
<script src="{{ asset('spc-b/js/main.min.js') }}"></script>
<script src="{{ asset('spc-b/js/custom.js') }}"></script>