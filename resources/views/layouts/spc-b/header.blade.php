<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Serena Prestige Club</title>
    <meta name="description" content="Serena Prestige Club">
    <meta name="keywords" content="Serena Prestige Club">
    
    <!-- Favicon -->
    <link rel="icon" href="{{ asset('spc-b') }}/img/favicon.ico" />

    <link href="https://fonts.googleapis.com/css?family=Crimson+Text:300,400,700|Rubik:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('spc-b/css/styles-merged.css') }}">
    <link rel="stylesheet" href="{{ asset('spc-b/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('spc-b/css/custom.css') }}">

    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.min.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <!-- START: header -->

  <header role="banner" class="probootstrap-header">
    <!-- <div class="container"> -->
    <div class="row">
        <a href="/en/home" class="probootstrap-logo visible-xs"><img src="{{ asset('spc-b') }}/img/logo.png" class="hires" width="120" height="33" alt="Serena Prestige Club"></a>
        
        <a href="#" class="probootstrap-burger-menu visible-xs"><i>Menu</i></a>
        <div class="mobile-menu-overlay"></div>

        <nav role="navigation" class="probootstrap-nav hidden-xs">
          <ul class="probootstrap-main-nav">
            <!-- Insert menu here -->
          @include('menu')
          <div class="extra-text visible-xs">
            <a href="#" class="probootstrap-burger-menu"><i>Menu</i></a>
            <h5>Connect With Us</h5>
            <ul class="social-buttons">
              <li><a href="#"><i class="icon-twitter"></i></a></li>
              <li><a href="#"><i class="icon-facebook2"></i></a></li>
              <li><a href="#"><i class="icon-instagram2"></i></a></li>
            </ul>
          </div>
        </nav>
        </div>
    <!-- </div> -->
  </header>
  <!-- END: header -->

  <section class="probootstrap-slider flexslider">
    <ul class="slides">
       <li style="background-image: url({{ asset('spc-b') }}/img/epic-sundowner.jpg);" class="overlay">
          <div class="container">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="probootstrap-slider-text text-center">
                  <p><img src="{{ asset('spc-b') }}/img/curve_white.svg" class="seperator probootstrap-animate" alt="Serena Prestige Club"></p>
                  <h1 class="probootstrap-heading probootstrap-animate">Exclusive privileges &amp; benefits</h1>
                  <div class="probootstrap-animate probootstrap-sub-wrap">As a member of the Serena Prestige Club, you enjoy exclusive privileges, benefits and discounts designed to make your stay extra special.</div>
                </div>
              </div>
            </div>
          </div>
        </li>
        <li style="background-image: url({{ asset('spc-b') }}/img/kirawira-camp-welcome.jpg);" class="overlay">
          <div class="container">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="probootstrap-slider-text text-center">
                  <p><img src="{{ asset('spc-b') }}/img/curve_white.svg" class="seperator probootstrap-animate" alt="Serena Prestige Club"></p>
                  <h1 class="probootstrap-heading probootstrap-animate">Enjoy Luxury Experience</h1>
                  <div class="probootstrap-animate probootstrap-sub-wrap">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                </div>
              </div>
            </div>
          </div>
          </li>
          <li style="background-image: url({{ asset('spc-b') }}/img/quetta-coffee-shop.jpg);" class="overlay">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="probootstrap-slider-text text-center">
                  <p><img src="{{ asset('spc-b') }}/img/curve_white.svg" class="seperator probootstrap-animate" alt="Serena Prestige Club"></p>
                  <h1 class="probootstrap-heading probootstrap-animate">Membership</h1>
                  <div class="probootstrap-animate probootstrap-sub-wrap">Your membership status exemplifies your importance to us and gives you assurance that you will always be treated as a prestigious guest of Serena.</div>
                </div>
              </div>
            </div>
          </div>
          </li>
          <li style="background-image: url({{ asset('spc-b') }}/img/turkish-design.jpg);" class="overlay">
          <div class="container">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="probootstrap-slider-text text-center">
                  <p><img src="{{ asset('spc-b') }}/img/curve_white.svg" class="seperator probootstrap-animate" alt="Serena Prestige Club"></p>
                  <h1 class="probootstrap-heading probootstrap-animate">Redeem Points</h1>
                  <div class="probootstrap-animate probootstrap-sub-wrap">Serena Prestige Club is just another way we want to say thank you, asante and shukraan for making us your preferred hotel.</div>
                </div>
              </div>
            </div>
          </div>
        </li>
         <li style="background-image: url({{ asset('spc-b') }}/img/selous-camp.jpg);" class="overlay">
          <div class="container">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="probootstrap-slider-text text-center">
                  <p><img src="{{ asset('spc-b') }}/img/curve_white.svg" class="seperator probootstrap-animate" alt="Serena Prestige Club"></p>
                  <h1 class="probootstrap-heading probootstrap-animate">Enjoy Luxury Experience</h1>
                  <div class="probootstrap-animate probootstrap-sub-wrap">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                </div>
              </div>
            </div>
          </div>
        </li>
    </ul>
  </section>

  <section class="probootstrap-section">
    <div class="container">
      <div class="row mb30">
        <div class="col-md-8 col-md-offset-2 probootstrap-section-heading text-center">
          <h2>Serena Prestige Club</h2>
          <p class="lead"> As a member of the Serena Prestige Club, you enjoy exclusive privileges, benefits and discounts designed to make your stay extra special. Your membership status exemplifies your importance to us and gives you assurance that you will always be treated as a prestigious guest of Serena.
</p><p class="lead">
Serena Prestige Club is just another way we want to say thank you, asante and shukraan for making us your preferred hotel.
</p><p class="lead">
Join today and start earning fabulous benefits and rewards..</p>
          <p><img src="{{ asset('spc-b') }}/img/curve.svg" class="svg" alt="Serena Prestige Club"></p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="service left-icon probootstrap-animate">
            <div class="icon">
              <img src="{{ asset('spc-b') }}/img/flaticon/svg/001-building.svg" class="svg" alt="Serena Prestige Club">
            </div>
            <div class="text">
              <h3>Find Your Hotel</h3>
              <p>A stay at a Serena Hotel offers guests a meticulously crafted retreat infused with the textures, flavours and architecture of the locale.</p>
              <p><a href="#" class="link-with-icon">Learn More <i class="icon-chevron-right"></i></a></p>
            </div>  
          </div>
        </div>
        <div class="col-md-4">
          <div class="service left-icon probootstrap-animate">
            <div class="icon">
              <img src="{{ asset('spc-b') }}/img/flaticon/svg/003-restaurant.svg" class="svg" alt="Serena Prestige Club">
            </div>
            <div class="text">
              <h3>Food &amp; Drinks</h3>
              <p>Each property in the Serena Hotels portfolio is as vibrant and unique as the region it serves, an authentic embodiment of its people, values, heritage and cuisine.</p>
              <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
            </div>  
          </div>
        </div>
        <div class="col-md-4">
          <div class="service left-icon probootstrap-animate">
            <div class="icon">
              <img src="{{ asset('spc-b') }}/img/flaticon/svg/binoculars.png" class="svg" alt="Serena Prestige Club">
            </div>
            <div class="text">
              <h3>Game Watching, Adventure</h3>
              <p>The 35 collection of Serena hotels in Africa and Asia, are in some of the world’s most interesting, enchanting, historic and exotic settings.</p>
              <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
            </div>  
          </div>
        </div>
      </div>
    </div>
  </section>

<!-- START: footer -->
  <footer role="contentinfo" class="probootstrap-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="probootstrap-footer-widget">
            <p class="mt40"><img src="{{ asset('spc-b') }}/img/serena-spa.jpg" class="hires" height="100" alt="Serena Prestige Club"></p>
            <p>Live a well-travelled life in style and experience authentic cuisines, indigenous aesthetics and spectacular delights in some of the world’s most extraordinary locations with Serena Hotels. Our diverse collection of thirty-five hotels, resorts, safari lodges, camps and forts within East Africa, Mozambique and South and Central Asia are for those who lean towards distinctive and inspirational experiences.</p>
            <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="probootstrap-footer-widget">
            <h3>Destinations</h3>
            <ul class="probootstrap-blog-list">
              <li>
                <a href="#">
                  <figure class="probootstrap-image"><img src="{{ asset('spc-b') }}/img/nairobi-serena-hotel.jpg" alt="Serena Prestige Club" class="img-responsive"></figure>
                  <div class="probootstrap-text">
                    <h4>Nairobi Serena Hotel</h4>
                    <span class="meta">Where to stay in Nairobi</span>
                    <p>Nairobi Serena Hotel enjoys a coveted location in the city centre, adjacent to Central Park</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="#">
                  <figure class="probootstrap-image"><img src="{{ asset('spc-b') }}/img/serena-beach.jpg" alt="Serena Prestige Club" class="img-responsive"></figure>
                  <div class="probootstrap-text">
                    <h4>Serena Beach Resort & Spa</h4>
                    <span class="meta">5 Star Mombasa Beach Resort</span>
                    <p>Guests arriving at Serena Beach Resort & Spa find an oasis of tranquillity framed by whispering coconut palms, lush gardens...</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="#">
                  <figure class="probootstrap-image"><img src="{{ asset('spc-b') }}/img/ole-pejeta.jpg" alt="Serena Prestige Club" class="img-responsive"></figure>
                  <div class="probootstrap-text">
                    <h4>Sweetwaters Serena Camp</h4>
                    <span class="meta">Ol Pejeta Conservancy Tented Safari Camp</span>
                    <p>Embrace the awe-inspiring natural beauty of your surroundings, and set the stage for a visit you won’t soon forget.</p>
                  </div>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-md-4">
          <div class="probootstrap-footer-widget">
            <h3>Contact</h3>
            <ul class="probootstrap-contact-info">
              <li><i class="icon-location2"></i> <span>George Williamson House 4th Floor, 4th Ngong Avenue P.O. BOX 48690 – 00100 Nairobi, Kenya</span></li>
              <li><i class="icon-mail"></i><span>info@serenaprestigeclub.com</span></li>
              <li><i class="icon-phone2"></i><span>+254 732 123 333; +254 709 998 333; +254 20 2842 333</span></li>
            </ul>
            
          </div>
        </div>
      </div>
      <div class="row mt40">
        <div class="col-md-12 text-center">
          <ul class="probootstrap-footer-social">
            <li><a href=""><i class="icon-twitter"></i></a></li>
            <li><a href=""><i class="icon-facebook"></i></a></li>
            <li><a href=""><i class="icon-instagram2"></i></a></li>
          </ul>
          <p>
            <small>&copy; 2019 <a href="https://www.serenahotels.com/" target="_blank">Tourism Promotion Services Kenya</a>. All Rights Reserved. <br> Designed &amp; Developed by <a href="https://www.innovationslate.com/" target="_blank">Innovation Slate Ltd</a></small>
          </p>
          
        </div>
      </div>
    </div>
  </footer>
  <!-- END: footer -->
  
  <script src="{{ asset('spc-b/js/scripts.min.js') }}"></script>
  <script src="{{ asset('spc-b/js/main.min.js') }}"></script>
  <script src="{{ asset('spc-b/js/custom.js') }}"></script>
  </body>
</html>