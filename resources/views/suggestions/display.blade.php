@extends('layouts.spc-b.inner')

@section('caption')
Serena Prestige Points
@endsection

@section('filename')
background-image: url({{ asset('spc-b') }}/img/header1.jpg);
@endsection

@section('description')
Redeem your accumulated points for a variety of benefits that you deserve.
@endsection

@section('content')
<style>

.price-booking-bar {

    width: 100%;
    height: 49px;
    background: #f6871f;
    margin: 10px auto 10px;
    box-sizing: border-box;
    padding: 11px 30px;
    float: left;

}

.offer {

}
</style>
<div class="row">
	@foreach($suggestions as $suggestion)
	<div class="col-md-4 offer border probootstrap-animate">
		<img width="240" src="{{ asset('uploads/' . $suggestion->file_name) }}" />
		<h4 style="margin-top:0px;text-transform: uppercase;font-weight: bold;">{{ $suggestion->title }}</h4>
		<p>{!! str_limit($suggestion->body, $limit = 100, $end = '...') !!}</p>
		<a style="color:orange;text-transform: uppercase; border-bottom:solid 2px orange;margin-bottom:50px;" href="{{ URL::to('/') }}/view/{{ $suggestion->slug }}" target="_self" tabindex="0">Read More</a>

		<div class="price-booking-bar flt-clr">							
<span class="offer-itm-booknow full-width">
<a style="text-transform: uppercase;color:white;font-weight: bold;" href="#" target="_self" tabindex="0">Book Now</a>
</span>	
					
</div>
	</div>
	@endforeach
</div>
@endsection