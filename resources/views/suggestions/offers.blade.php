<style>

.price-booking-bar {

    width: 100%;
    height: 49px;
    background: #f6871f;
    margin: 10px auto 10px;
    box-sizing: border-box;
    padding: 11px 30px;
    float: left;

}

</style>
<div class="row">
	<h2>Prestige Club Offers</h2>
	@foreach($offers as $offer)
	@if($loop->iteration  < 4)
	<div class="col-md-4 offer border probootstrap-animate">
		<img width="240" src="{{ asset('uploads/' . $offer->file_name) }}" />
		<h4 style="margin-top:0px;text-transform: uppercase;font-weight: bold;">{{ $offer->title }}</h4>
		<p>{!! str_limit($offer->body, $limit = 100, $end = '...') !!}</p>
		<a style="color:orange;text-transform: uppercase; border-bottom:solid 2px orange;margin-bottom:50px;" href="{{ URL::to('/') }}/view/{{ $offer->slug }}" target="_self" tabindex="0">Read More</a>

		<div class="price-booking-bar flt-clr">							
		<span class="offer-itm-booknow full-width">
		<a style="text-transform: uppercase;color:white;font-weight: bold;" href="#" target="_self" tabindex="0">Book Now</a>
		</span>	
							
		</div>
	</div>
	@endif
	@endforeach
</div>