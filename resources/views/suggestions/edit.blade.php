@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit Menu Item
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" enctype="multipart/form-data" action="{{ route('suggestions.update', $suggestion->id) }}">
        @method('PATCH')
        @csrf
        @include('suggestions.form')
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
  </div>
</div>
@endsection