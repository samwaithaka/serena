	@foreach($suggestions as $suggestion)
        <li style="background-image: url({{ asset('uploads/' . $suggestion->file_name) }});" class="overlay">
          <div class="container">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="probootstrap-slider-text text-center">
                  <h1 class="probootstrap-heading probootstrap-animate">{{ $suggestion->title }}</h1>
                  <div class="probootstrap-animate probootstrap-sub-wrap">{{ $suggestion->body }}</div>
                </div>
              </div>
            </div>
          </div>
        </li>
	@endforeach