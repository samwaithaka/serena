@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <a href="{{$suggestions[0]->type_id}}/create" class="btn btn-primary">Add Item</a>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Offer Title</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($suggestions as $suggestion)
        <tr>
            <td>{{$suggestion->id}}</td>
            <td>{{$suggestion->title}}</td>
            <td><a href="{{ route('suggestions.edit',$suggestion->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('suggestions.destroy', $suggestion->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection