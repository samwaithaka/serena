@extends('layouts.spc-b.inner')

@section('caption')
Serena Prestige Points
@endsection

@section('filename')
background-image: url({{ asset('spc-b') }}/img/header2.jpg);
@endsection

@section('description')
Redeem your accumulated points for a variety of benefits that you deserve.
@endsection


@section('content')
<style>

.price-booking-bar {

    width: 100%;
    height: 49px;
    background: #f6871f;
    margin: 10px auto 10px;
    box-sizing: border-box;
    padding: 11px 30px;
    float: left;
}

.offer {

}
</style>
<div class="row">
	<div class="col-md-12">
		<img class="probootstrap-animate" width="700" src="{{ asset('uploads/' . $suggestion->file_name) }}" />
		<h2 class="probootstrap-animate">{{ $suggestion->title }}</h2>
		<p class="probootstrap-animate">{!! $suggestion->body !!}</p>
	</div>
	<div>
		<div class="col-md-4"></div>
		<div class="col-md-4 price-booking-bar flt-clr">							
			<span class="offer-itm-booknow full-width">
			<a style="text-transform: uppercase;color:white;font-weight: bold;" href="#" target="_self" tabindex="0">Book Now</a>
			</span>				
        </div>
        <div class="col-md-4"></div>
	</div>
</div>
@endsection