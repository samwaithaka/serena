<div class="form-group">
  @csrf
  <label for="name">Title:</label>
  <input type="text" class="form-control" name="title" value={{ $suggestion->title }}></input>
</div>
<div class="form-group">
  <label for="name">Description:</label>
  <textarea class="form-control" name="description">{{ $suggestion->description }}</textarea>
</div>
<div class="form-group">
  <label for="name">Body:</label>
  <textarea class="form-control" name="body">{{ $suggestion->body }}</textarea>
</div>
<div class="form-group">
  <label for="name">Link:</label>
  <input type="text" class="form-control" name="link" value={{ $suggestion->link }}></input>
</div>
<div class="form-group">
  <label for="name">Poster:</label>
  <input data-preview="#preview" name="poster" type="file" id="poster" />
</div>

<input type="hidden" name="type_id" value={{ $suggestion->type_id }}></input>