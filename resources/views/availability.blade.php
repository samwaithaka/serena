<style>
    .availability-form {
        background:#fff;
        text-align: left;
        font-size:75%;
        margin:-25px auto;
        position:fixed;
        left:0px;
        height:100%;
        width: 100%;
        visibility:visible;
    }

  .reserve {
    background: #2A120A;
    position:relative;
  }

  .availability-form {
    height:100px;
  }

  .reserve {
    height: 100px;
  }

  select, input, option {
    height:40px;
    font-size: 0.8em;
  }

  label {
    color:#ffffff;
  }

  .form-field {
    font-size: 0.8em;
  }
</style>

<div class="container availability-form clearfix">
  <form id="booking-form" class="booking-mask probootstrap-form" action="https://bookings.ihotelier.com/bookings.jsp" data-action="http://reservations.serenahotels.co.ke/index.php" method="get" target="_blank">
    <div class="row reserve">
    <div class="col-md-2 form-group">
      <label for="hotel">Select Hotel</label>
      <div class="form-field">
        <i class="icon icon-chevron-down"></i>
        <select id="property" name="hotelid" class="form-control">
    <option value="group" selected="selected">Select Hotel</option>
    <optgroup class="continent" label="Africa"></optgroup>

    <optgroup class="country" label="Kenya">
        <option value="17000" class="form-nairobi" data-propertyid="serenanairobi">Nairobi Serena Hotel
        </option>
        <option value="17001" class="form-beachhotel" data-propertyid="serenabeachhotel">Serena Beach Resort &amp; Spa
        </option>
        <option value="74396" class="form-sweetwaters" data-propertyid="serenasweetwaters">Sweetwaters Serena Camp
        </option>
        <option value="76061" class="form-elmenteita" data-propertyid="serenaelmenteita">Lake Elmenteita Serena Camp
        </option>
        <option value="74417" class="form-mara" data-propertyid="serenamara">Mara Serena Safari Lodge
        </option>
        <option value="74405" class="form-amboseli" data-propertyid="serenaamboseli">Amboseli Serena Safari Lodge
        </option>
        <option value="74411" class="form-kilaguni" data-propertyid="serenakilaguni">Kilaguni Serena Safari Lodge
        </option>
        <option value="74407" class="form-mountainlodge" data-propertyid="serenamountainlodge">Serena Mountain Lodge
        </option>
        <option value="" class="form-olpejeta" data-propertyid="serenaolpejeta">Ol Pejeta House
        </option>
    </optgroup>
    <optgroup class="country" label="Rwanda">
        <option value="17004" class="form-kigali" data-propertyid="serenakigali">Kigali Serena Hotel
        </option>
        <option value="17005" class="form-lakekivu" data-propertyid="serenalakekivu">Lake Kivu Serena Hotel
        </option>
    </optgroup>
    <optgroup class="country" label="Uganda">
        <option value="17007" class="form-kampala" data-propertyid="serenakampala">Kampala Serena Hotel
        </option>
        <option value="74368" class="form-lakevictoria" data-propertyid="serenalakevictoria">Lake Victoria Serena Golf Resort &amp; Spa
        </option>
    </optgroup>
    <optgroup class="country" label="Tanzania">
        <option value="76352" class="form-daressalaam" data-propertyid="serenadaressalaam">Dar Es Salaam Serena Hotel
        </option>
        <option value="17003" class="form-zanzibar" data-propertyid="serenazanzibar">Zanzibar Serena Hotel
        </option>
        <option value="17002" class="form-arusha" data-propertyid="serenaarusha">Arusha Serena Hotel, Resort &amp; Spa
        </option>
        <option value="74414" class="form-mbuzi" data-propertyid="serenambuzi">Mbuzi Mawe Serena Camp
        </option>
        <option value="74415" class="form-kirawira" data-propertyid="serenakirawira">Kirawira Serena Camp
        </option>
        <option value="74416" class="form-selous" data-propertyid="serenaselous">Selous Serena Camp
        </option>
        <option value="74413" class="form-ngorongoro" data-propertyid="serenangorongoro">Ngorongoro Serena Safari Lodge
        </option>
        <option value="74400" class="form-serengeti" data-propertyid="serenaserengeti">Serengeti Serena Safari Lodge
        </option>
        <option value="74409" class="form-lakemanyara" data-propertyid="serenalakemanyara">Lake Manyara Serena Safari Lodge
        </option>
        <option value="74403" class="form-mivumo" data-propertyid="serenamivumo">Serena Mivumo River Lodge
        </option>
    </optgroup>
    <optgroup class="country" label="Mozambique">
        <option value="17006" class="form-polana" data-propertyid="serenapolana">Polana Serena Hotel
        </option>
    </optgroup>
    <optgroup class="continent" label="Asia"></optgroup>

    <optgroup class="country" label="Afghanistan">
        <option value="17008" class="form-kabul" data-propertyid="serenakabul">Kabul Serena Hotel
        </option>
    </optgroup>
    <optgroup class="country" label="Pakistan">
        <option value="17009" class="form-islamabad" data-propertyid="serenaislamabad">Islamabad Serena Hotel
        </option>
        <option value="74359" class="form-quetta" data-propertyid="serenaquetta">Quetta Serena Hotel
        </option>
        <option value="74357" class="form-faisalabad" data-propertyid="serenafaisalabad">Faisalabad Serena Hotel
        </option>
        <option value="74367" class="form-shigarf" data-propertyid="serenashigarf">Serena Shigar Fort
        </option>
        <option value="74365" class="form-hunza" data-propertyid="serenahunza">Hunza Serena Inn
        </option>
        <option value="76351" class="form-khaplupalace" data-propertyid="serenakhaplupalace">Serena Khaplu Palace
        </option>
        <option value="74361" class="form-swat" data-propertyid="serenaswat">Swat Serena Hotel
        </option>
        <option value="74363" class="form-gilgit" data-propertyid="serenagilgit">Gilgit Serena Hotel
        </option>
    </optgroup>
    <optgroup class="country" label="Tajikistan">
        <option value="76338" class="form-dushanbe" data-propertyid="serenadushanbe">Dushanbe Serena Hotel
        </option>
        <option value="" class="form-khorog" data-propertyid="serenakhorog">Khorog Serena Inn
        </option>
    </optgroup>
</select>
      </div>
    </div>
        <div class="col-md-2 form-group">
          <label for="check-in">Check In</label>
          <div class="form-field">
            <i class="icon icon-calendar2"></i>
            <input type="text" class="form-control" id="check-in" name="check-in" />
          </div>
        </div>
      
        <div class="col-md-2 form-group">
          <label for="check-out">Check Out</label>
          <div class="form-field">
            <i class="icon icon-calendar2"></i>
            <input type="text" class="form-control" id="check-out" name="check-out" />
          </div>
        </div>    
        <div class="col-md-1 form-group">
          <label for="rooms">No. of Rooms</label>
          <div class="form-field">
            <i class="icon icon-chevron-down"></i>
            <select name="rooms" id="rooms" class="form-control">
                <option selected="selected" value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
            </select>
          </div>
        </div>

        <div class="col-md-1 form-group">
          <label for="adults">Adults</label>
          <div class="form-field">
            <i class="icon icon-chevron-down"></i>
            <select name="adults" id="adults" class="form-control">
                <option selected="selected">1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
                <option>11</option>
                <option>12</option>
            </select>
          </div>
        </div>
      
        <div class="col-md-1 form-group">
          <label for="children">Children</label>
          <div class="form-field">
            <i class="icon icon-chevron-down"></i>
            <select name="children" id="children" class="form-control">
                <option selected="selected">0</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
            </select>
          </div>
        </div>
    <div class="col-md-1 form-group">
      <label for="submit">&nbsp;</label>
      <input type="submit" class="book btn btn-primary btn-lg" id="submit" name="submit" value="Check Rates">
    </div>
</div>
</form>
</div>