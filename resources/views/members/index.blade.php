@extends('layouts.app')

@section('member')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Name</td>
          <td>CNIC</td>
          <td>Email Address</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($members as $member)
        <tr>
            <td>{{$member->name}}</td>
            <td>{{$member->cnic}}</td>
            <td>{{$member->email_address}}</td>
            <td><a href="{{ route('members.edit',$member->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('members.destroy', $member->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection