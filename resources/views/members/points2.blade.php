@extends('layouts.spc-b.inner')

@section('content')

<?php $required = 0 ?>

<?php $balance = 1 ?>

<?php print_r($data); ?>
<?php $msg = '' ?>

@if($balance > $required)
   <?php $needed =  round($required - $balance,1) ?>
   <?php $msg =  
   '<p>Your current progress to becoming a&nbsp;<a href="#">Platinum Member</a>.&nbsp;</p>
   <p>You have&nbsp;<span style="text-decoration: underline;">' . $balance . ' Points</span> and you currently need <span style="text-decoration: underline;">' . $needed . ' Points</span> more to go.</p>' ?>
@else
   <?php $msg =  '<p>Congratulations you are a&nbsp;<a href="#">Platinum Member</a>.</p>' ?>
@endif

<?php echo $msg ?>
@endsection