@extends('layouts.spc-b.inner')

@section('content')
<style>
  .uper {
    margin-top: 20px;
    text-align: left;
  }
  .left {
    text-align:left;
  }
  label {
    margin-bottom: 2px;
    font-weight: 700;
  }
</style>
<div class="card uper col-md-7">
  <h2>Enrol</h2>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('members.store') }}">
          @include('members.form')
          <br />
          <button type="submit" class="btn btn-primary">Submit</button>
      </form>
  </div>
</div>
<div class="col-md-5 left">
    <p class="mt40"><img src="{{ asset('spc-b') }}/img/serena-beach.jpg" class="hires" height="250" alt="Serena Prestige Club"></p>
    <p>Live a well-travelled life in style and experience authentic cuisines, indigenous aesthetics and spectacular delights in some of the world’s most extraordinary locations with Serena Hotels. Our diverse collection of thirty-five hotels, resorts, safari lodges, camps and forts within East Africa, Mozambique and South and Central Asia are for those who lean towards distinctive and inspirational experiences.</p>
</div>
@endsection