<p>
  You can enroll to become a Prestige Club member through our online booking form below.
</p>
<div class="form-group">
  @csrf
  <label for="name">Name:</label>
  <input type="text" class="form-control col-sm-6" name="name" required="required" value={{ $member->name }} ></input>
</div>
<div class="form-group">
  <label for="name">ID/Passport No:</label>
  <input type="text" class="form-control col-sm-6" name="cnic" required="required" value={{ $member->cnic }} ></input>
</div>
<div class="form-group">
  <label for="company_name">Company Name:</label>
  <input type="text" class="form-control col-sm-6" name="company_name" required="required" value={{ $member->company_name }} ></input>
</div>
<div class="form-group">
  <label for="email_address">Email Address:</label>
  <input type="email" class="form-control col-sm-6" required="required" name="email_address" value={{ $member->email_address }}></input>
</div>
<div class="form-group">
  <label for="password">Password:</label>
  <input type="password" class="form-control col-sm-6" required="required" name="password" value={{ $member->password }}></input>
</div>
<div class="form-group">
  <label for="date_of_birth">Date of Birth:</label>
  <input type="text" class="form-control col-sm-6" required="required" name="date_of_birth" required="required" placeholder="YYYY-MM-DD" value={{ $member->date_of_birth }}></input>
</div>
<div class="form-group">
  <label for="marital_status">Marital Status:</label>
   <select class="form-control col-sm-4" name="marital_status">
    <option value="Single" @if($member->marital_status == "Single") selected @endif>
      Single
    </option>
    <option value="Married" @if($member->marital_status == "Married") selected @endif>
      Married
    </option>
    <option value="Separated" @if($member->marital_status == "Separated") selected @endif>
      Separated
    </option>
  </select>
</div>
<div class="form-group">
  <label for="gender">Gender:</label>
  <select class="form-control col-sm-4" name="gender">
    <option value="Male" @if($member->gender == "Male") selected @endif>
      Male
    </option>
     <option value="Female" @if($member->gender == "Female") selected @endif>
      Female
    </option>
  </select>
</div>
<div class="form-group">
  <label for="name">Residence Address:</label>
  <input type="text" class="form-control col-sm-6" required="required" name="residence_address" value={{ $member->recidence_address }}></input>
</div>
<div class="form-group">
  <label for="name">Residence City:</label>
  <input type="text" class="form-control col-sm-6" required="required" name="residence_city" value={{ $member->recidence_city }}></input>
</div>
<div class="form-group">
  <label for="name">Residence Province:</label>
  <input type="text" class="form-control col-sm-6" required="required" name="residence_province" value={{ $member->residence_province }}></input>
</div>
<div class="form-group">
  <label for="intro">Residence Country:</label>
  <select class="form-control col-sm-4" name="residence_country">
      @foreach($countries as $country)
      <option value="{{$country->country_code}}" @if($member->recidence_country === $country->country_code) selected @endif>
        {{$country->country_name}}
      </option>
    @endforeach
  </select>
</div>
<div class="form-group">
  <label for="name">Mobile Number:</label>
  <input type="text" class="form-control col-sm-6" required="required" name="mobile_number" value={{ $member->mobile_number }}></input>
</div>
<div class="form-group">
  <label for="name">Preferred Number:</label>
  <input type="text" class="form-control col-sm-6" required="required" name="preferred_number" value={{ $member->preferred_number }}></input>
</div>
<div class="form-group">
<label for="name">I agree to the terms and conditions:</label>
  <input type="checkbox" class="form-control col-sm-1" required="required" name="terms"></input>
</div>