@extends('layouts.spc-b.inner')

@section('title')
Check your points
@endsection

@section('caption')
Prestige points
@endsection

@section('filename')
background-image: url({{ asset('spc-b') }}/img/turkish-design.jpg);
@endsection

@section('description')
Your current progress to becomin
@endsection

@section('content')

<?php $required = 0 ?>

<?php $balance = round($data['CardBalance'],1) ?>


<?php $msg = '<p>Your current progress to becoming a&nbsp;<a href="#">Platinum Member</a>.&nbsp;</p><p>You have&nbsp;<span style="text-decoration: underline;">' . $balance . ' points</span> .</p>'; ?>
<?php $msg = '<div id="dash_detail_top">
      <div class="moduletable">
	<h3 style="text-align:left;">My Account Summary</h3>
	<p style="margin-top: 0px; margin-bottom: 8px; text-align: justify; line-height: 16px; padding: 0px;"><strong>Name: </strong>' . $data['CardHolderName'] . '</p>
        <p style="margin-top: 0px; margin-bottom: 8px; text-align: justify; line-height: 16px; padding: 0px;"><strong>Earned Points: </strong>' . number_format(round($data['CardBalance'],1), 1 , '.' , ',') . '</p>
        <p style="margin-top: 0px; margin-bottom: 8px; text-align: justify; line-height: 16px; padding: 0px;"><strong>Member Number: </strong>' .  $data['MembershipNumber'] . '</p>
      </div>
</div>';
echo $msg;
?>
<?php $count = count($data['Tran']) ?>
<?php $row = 0 ?>

<?php $table_start = "<table>
                    <tr>
                        <td colspan='7'><h3 style='text-align:left;'>My Recent Activity</h3></td>
                    </tr>
                    <tr>
                        <th>Transaction Date</th>
                        <th>Total Amount</th>
                        <th>Transaction Type</th>
                        <th>Points</th>
                        <th>Merchant Name</th>
                        <th>Outlet Name</th>
                        <th>Product Name</th>
                    </tr>" ?>
<?php $table_end = "</table>" ?>
<?php $table_row = '' ?>
<?php $table = '' ?>


    <?php $i = 1 ?>
    @foreach($data['Tran'] as $row)
        @if($i <= 5 && strlen($row)>0)
        <?php $r = explode(",",$row) ?>
        <?php $table_row .= "<tr><td> <?php $r[0] ?></td>
                           <td style='text-align:right;padding:0 10px 0 0;'>".number_format($r[1], 2 , '.' , ',')."</td>
                           <td>".$r[2]."</td>
                           <td style='text-align:right;padding:0 10px 0 0;'>".number_format($r[3], 2 , '.' , ',')."</td>
                           <td>".$r[4]."</td>
                           <td>".$r[5]."</td>
                           <td>".$r[6]."</td>
                      </tr>" ?>
        @endif
        <?php $i++; ?>
    @endforeach

@if($table_row != '')
    <?php echo $table_start.$table_row.$table_end; ?>
@else
    <?php echo "<h3>You have not made any transactions yet.</h3>"; ?>
@endif

@endsection