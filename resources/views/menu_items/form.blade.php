<div class="form-group">
  @csrf
  <label for="name">Title:</label>
  <input type="text" class="form-control" name="title" value={{ $menuItem->title }}></input>
</div>
<div class="form-group">
  <label for="name">URI:</label>
  <input type="text" class="form-control" name="uri" value={{ $menuItem->uri }}></input>
</div>
<input type="hidden" name="menu_id" value={{ $menuItem->menu_id }}></input>