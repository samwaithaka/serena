@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <a href="{{$menuItems[0]->menu_id}}/create" class="btn btn-primary">Add Item</a>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Menu Item Title</td>
          <td>URI</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($menuItems as $menuItem)
        <tr>
            <td>{{$menuItem->id}}</td>
            <td>{{$menuItem->title}}</td>
            <td>{{$menuItem->uri}}</td>
            <td><a href="{{ route('menu-items.edit',$menuItem->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('menu-items.destroy', $menuItem->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection