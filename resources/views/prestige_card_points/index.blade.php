@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <a href="{{$prestigeCardPoints[0]->hotel_id}}/create" class="btn btn-primary">Add Item</a>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Room Type</td>
          <td>Points</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($prestigeCardPoints as $prestigeCardPoint)
        <tr>
            <td>{{$prestigeCardPoint->id}}</td>
            <td>{{$prestigeCardPoint->room_type}}</td>
            <td>{{$prestigeCardPoint->points}}</td>
            <td><a href="{{ route('prestige-card-points.edit',$prestigeCardPoint->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('prestige-card-points.destroy', $prestigeCardPoint->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection