<div class="form-group">
  @csrf
  <label for="name">Room Type:</label>
  <input type="text" class="form-control" name="room_type" value={{ $prestigeCardPoint->room_type }}></input>
</div>
<div class="form-group">
  <label for="name">Low Season:</label>
  <input type="text" class="form-control" name="low_season_points" value={{ $prestigeCardPoint->low_season_points }}></input>
</div>
<div class="form-group">
  <label for="name">Mid Season:</label>
  <input type="text" class="form-control" name="mid_season_points" value={{ $prestigeCardPoint->mid_season_points }}></input>
</div>
<div class="form-group">
  <label for="name">High Season:</label>
  <input type="text" class="form-control" name="high_season_points" value={{ $prestigeCardPoint->high_season_points }}></input>
</div>
<div class="form-group">
  <label for="name">Peak Season:</label>
  <input type="text" class="form-control" name="peak_season_points" value={{ $prestigeCardPoint->peak_season_points }}></input>
</div>
<div class="form-group">
  <label for="name">Upgrade Points:</label>
  <input type="text" class="form-control" name="upgrade_points" value={{ $prestigeCardPoint->upgrade_points }}></input>
</div>
<div class="form-group">
  <label for="name">Remarks:</label>
  <input type="text" class="form-control" name="remarks" value={{ $prestigeCardPoint->remarks }}></input>
</div>
<input type="hidden" name="hotel_id" value={{ $prestigeCardPoint->hotel_id }}></input>