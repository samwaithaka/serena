<?php

use App\Tools\MemberRegistration;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('/en/home');
});

//Route::get('/en/home', function () { return view('index');});
Route::get('/en/home', ['uses' =>'HomeController@index']);
Route::get('/en/{slug}', ['uses' =>'ContentController@page']);
Route::get('/display/{key}', ['uses' =>'SuggestionController@display']);
Route::get('/view/{slug}', ['uses' =>'SuggestionController@view']);
Route::get('/member/points', ['uses' =>'MemberController@checkPoints']);
Route::get('/serena-hotels/points', ['uses' =>'PrestigeCardHotelController@find']);

Route::get('/menu-items/{menu_id}/create', ['uses' =>'MenuItemController@create']);
Route::get('/menu-items/{menu_id}', ['uses' =>'MenuItemController@index']);
Route::get('/suggestions/{type_id}/create', ['uses' =>'SuggestionController@create']);
Route::get('/suggestions/{type_id}', ['uses' =>'SuggestionController@index']);
Route::get('/prestige-card-hotels/find', ['uses' =>'PrestigeCardHotelController@find']);	
Route::get('/prestige-card-points/{hotel_id}/create', ['uses' =>'PrestigeCardPointController@create']);
Route::get('/prestige-card-points/{hotel_id}', ['uses' =>'PrestigeCardPointController@index']);
Route::get('/prestige-card-meal-points/{hotel_id}/create', ['uses' =>'PrestigeCardMealPointController@create']);
Route::get('/prestige-card-meal-points/{hotel_id}', ['uses' =>'PrestigeCardMealPointController@index']);

Route::get('/members/enrol', ['uses' =>'MemberController@enrol']);

Route::get('/confirm/success', function () { 
	return redirect('/confirm')->with('success', 'Member has been successfully enrolled');
});
Route::get('/confirm/error', function () { 
	return redirect('/confirm')->with('error', 'Member not enrolled, an error occured!');
});

Route::get('/confirm', function () { return view('members.confirm');});

Route::get('/points2', function () { return view('members.points');});

Route::resource('prestige-card-hotels', 'PrestigeCardHotelController');
Route::resource('prestige-card-points', 'PrestigeCardPointController');
Route::resource('prestige-card-meal-points', 'PrestigeCardMealPointController');
Route::resource('menu-items', 'MenuItemController');
Route::resource('menus', 'MenuController');
Route::resource('suggestions', 'SuggestionController');
Route::resource('suggestion-types', 'SuggestionTypeController');
Route::resource('content-types', 'ContentTypeController');
Route::resource('contents', 'ContentController');
Route::resource('countries', 'CountryController');
Route::resource('members', 'MemberController');
Route::resource('admin/members', 'MemberController');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'AdminController@index')->name('admin');