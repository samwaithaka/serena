<?php

use Illuminate\Http\Request;
use App\Http\Controllers\MemberController;
use App\Tools\MemberRegistration;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/rec', function () {
	$response = 
<<<STRIN
a:7:{s:6:"Status";s:2:"E0";s:16:"MembershipNumber";s:16:"1103000010025773";s:10:"CardNumber";s:16:"1103000010025773";s:14:"CardHolderName";s:15:"Samuel Waithaka";s:4:"CNIC";s:8:"20762033";s:12:"EmailAddress";s:19:"sam9@nextramile.com";s:8:"Password";s:40:"qHbfcu0DMZdSGZmWsmu9CacJ2JaTSbvf5Z7Kojoe";}
STRIN;
    $response = unserialize($response);
    $result = MemberRegistration::updateOnLocal($response);
	if($result == true)
		return redirect('/confirm/success');
	else
	    return redirect('/confirm/error');
});

Route::post('registration', function () {
	$result = MemberRegistration::updateOnLocal($_POST);
	if($result == true)
		return redirect('/confirm/success');
	else
	    return redirect('/confirm/error');
});

Route::post('points', function () {
	return view('members.points', ['data'=> $_POST]);
});