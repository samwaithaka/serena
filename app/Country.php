<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
    'country_name',
    'country_code',
    'low_season',
    'mid_season',
    'high_season',
    'peak_season'
  ];
}
