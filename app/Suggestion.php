<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    protected $fillable = [
        'type_id',
        'title',
        'description',
        'body',
        'slug',
        'file_name',
        'link'
  ];
}
