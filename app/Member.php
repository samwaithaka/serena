<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
    'cnic',
    'name',
    'first_name',
    'last_name',
    'password',
    'date_of_birth',
    'marital_status',
    'gender',
    'residence_address',
    'residence_city',
    'residence_province',
    'residence_country',
    'office_name',
    'designation',
    'office_address',
    'office_city',
    'office_province',
    'office_country',
    'home_phone',
    'mobile_number',
    'office_phone',
    'fax',
    'email_address',
    'mailing_address',
    'company_name',
    'preferred_number',
    'membership_number',
    'status'
  ];
}
