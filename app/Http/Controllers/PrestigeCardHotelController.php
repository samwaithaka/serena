<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\PrestigeCardHotel;
use App\Country;
use Image;

class PrestigeCardHotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prestigeCardHotels = PrestigeCardHotel::all();
        return view('prestige_card_hotels.index', compact('prestigeCardHotels'));
    }


public function show(Request $request) {
    $countryName = $request->get('country');
    $hotelName = $request->get('hotel');
    $countryList1 = DB::table('countries')->select('country_name')->get();
    $hotelList1 = DB::table('prestige_card_hotels')->select('hotel_name')
      ->join('countries','prestige_card_hotels.country_id','countries.id')
      ->orWhere('countries.country_name', 'like', '%' . $countryName . '%')
      ->get();

    $hotelsSeasons = DB::table('countries')->select('low_season','mid_season','high_season','peak_season')
      ->where('country_name', 'like', $countryName)
      ->get();
      //print "<pre>"; print_r($hotelsSeasons);exit;
      if (!$hotelName) {
          //$hotelName = "None";
      }
      if(!empty($hotelName) && empty($countryName)) {
          //$countryName = "None";
      }

      if(empty($hotelList1)) {
        //$hotelName = "None";
      }
//echo $hotelName.$countryName;
      if(!empty($hotelName) && !empty($countryName)) {
        //echo "yes";exit;
          $hotelsPoints = DB::table('prestige_card_hotels')->select('prestige_card_hotels.remarks as remarks', 'hotel_name', 'mode', 'prestige_card_points.hotel_id', 'country_name', 'prestige_card_points.remarks as more_remarks','low_season_points','mid_season_points','high_season_points','peak_season_points','upgrade_points','points','room_type')
      ->join('prestige_card_points','prestige_card_hotels.id','prestige_card_points.hotel_id')
      ->join('countries','prestige_card_hotels.country_id','countries.id')
      ->where('prestige_card_hotels.hotel_name', 'like', '%' . $hotelName . '%')
      ->orderByRaw('prestige_card_hotels.id ASC')
      ->get();

      $hotelsMealPoints = DB::table('prestige_card_hotels')->select('hotel_id','hotel_name','country_name','breakfast_points','lunch_points','dinner_points')
      ->join('prestige_card_meal_points','prestige_card_hotels.id','prestige_card_meal_points.hotel_id')
      ->join('countries','prestige_card_hotels.country_id','countries.id')
      ->where('prestige_card_hotels.hotel_name', 'like', '%' . $hotelName . '%')
      ->orderByRaw('prestige_card_hotels.id ASC')
      ->get();

      $hotelsSeasons = DB::table('prestige_card_hotels')->select('prestige_card_hotels.id as hotel_id','hotel_name','low_season','mid_season','high_season','peak_season')
      ->join('countries','prestige_card_hotels.country_id','countries.id')
      ->where('prestige_card_hotels.hotel_name', 'like', '%' . $hotelName . '%')
      ->orderByRaw('prestige_card_hotels.id ASC')
      ->get();
      } else {
        //echo "no"; exit;
        $hotelName = "None";
        $hotelsPoints = DB::table('prestige_card_hotels')->select('prestige_card_hotels.remarks as remarks', 'hotel_name', 'mode', 'prestige_card_points.hotel_id', 'country_name', 'prestige_card_points.remarks as more_remarks','low_season_points','mid_season_points','high_season_points','peak_season_points','upgrade_points','points','room_type')
      ->join('prestige_card_points','prestige_card_hotels.id','prestige_card_points.hotel_id')
      ->join('countries','prestige_card_hotels.country_id','countries.id')
      ->where('prestige_card_hotels.hotel_name', 'like', '%' . $hotelName . '%')
      ->orWhere('countries.country_name', 'like', '%' . $countryName . '%')
      //->orderBy =Raw('countries.id - prestige_card_hotels.id ASC')
      ->orderByRaw('prestige_card_hotels.id ASC')
      ->get();

      $hotelsMealPoints = DB::table('prestige_card_hotels')->select('hotel_id','hotel_name','country_name','breakfast_points','lunch_points','dinner_points')
      ->join('prestige_card_meal_points','prestige_card_hotels.id','prestige_card_meal_points.hotel_id')
      ->join('countries','prestige_card_hotels.country_id','countries.id')
      ->where('prestige_card_hotels.hotel_name', 'like', '%' . $hotelName . '%')
      ->orWhere('countries.country_name', 'like', '%' . $countryName . '%')
      ->orderByRaw('prestige_card_hotels.id ASC')
      ->get();
      }

      $hotels = array();
      $i = 0;      
      foreach ($hotelsPoints as $hotelsPoint) {
        $hotelId = $hotelsPoint->hotel_id;
        $hotels[$hotelId][0] = $hotelsPoint->hotel_name;
        $hotels[$hotelId][1] = $hotelsPoint->remarks;
        $hotels[$hotelId][2] = $hotelsPoint->country_name;
        foreach ($hotelsPoint as $key => $value) {
          if($key == 'room_type' || $key == 'low_season_points'|| $key == 'mid_season_points'
            || $key == 'high_season_points'|| $key == 'peak_season_points' 
            || $key == 'upgrade_points' || $key == 'points') {
              $hotels[$hotelId][3][$i][] = $value;
          }
        }
        $hotels[$hotelId][4] = $hotelsPoint->mode;
        ++$i;
      }
      foreach ($hotelsMealPoints as $mealsPoint) {
          $hotelId = $mealsPoint->hotel_id;
          foreach ($mealsPoint as $key => $value) {
              if($key == 'breakfast_points' || $key == 'lunch_points'|| $key == 'dinner_points') {
                  $hotels[$hotelId][5][] = $value;
              }
          }
      }

      $countryList = array();
      foreach ($countryList1 as $value) {
        $countryList[] = $value->country_name;
      }
      $hotelList = array();
      foreach ($hotelList1 as $value) {
        $hotelList[] = $value->hotel_name;
      }
      return view('prestige_card_hotels.hotels', compact('hotels','countryList','hotelList','countryName','hotelName','hotelsSeasons'));
}



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        $prestigeCardHotel = new PrestigeCardHotel();
        return view('prestige_card_hotels.create', compact('prestigeCardHotel','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'hotel_name' => 'required',
            'remarks' => 'required'
        ]);
        $prestigeCardHotel = new PrestigeCardHotel([
            'hotel_name' => $request->get('hotel_name'),
            'mode' => $request->get('mode'),
            'country_id' => $request->get('country_id'),
            'remarks' => $request->get('remarks')
        ]);
        $prestigeCardHotel->save();
        /*if ($request->hasFile('input_img')) {
            if($request->file('input_img')->isValid()) {
                try {
                    $file = $request->file('input_img');
                    $name = time() . '.' . $file->getClientOriginalExtension();
                    $img = Image::make(public_path($request->file('input_img')));
                    $img->resize(25, 25);
                    $img->save(public_path("/uploads/" . $name));
                    echo "file saved...";exit;
                } catch (Illuminate\Filesystem\FileNotFoundException $e) {
                }
            } 
        }*/
        return redirect('/prestige-card-hotels')->with('success', 'Hotel has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function find(Request $request)
    {
        $countryName = $request->get('country');
        $hotelName = $request->get('hotel');
        $countryList1 = DB::table('countries')->select('country_name')->get();
        $hotelList1 = DB::table('prestige_card_hotels')->select('hotel_name')
          ->join('countries','prestige_card_hotels.country_id','countries.id')
          ->orWhere('countries.country_name', 'like', '%' . $countryName . '%')
          ->get();
          $countryList = array();
      foreach ($countryList1 as $value) {
        $countryList[] = $value->country_name;
      }
      $hotelList = array();
      foreach ($hotelList1 as $value) {
        $hotelList[] = $value->hotel_name;
      }
        return view('prestige_card_hotels.find', compact('countryList','hotelList','countryName','hotelName'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countries = Country::all();
        $prestigeCardHotel = PrestigeCardHotel::find($id);
        return view('prestige_card_hotels.edit', compact('prestigeCardHotel','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'hotel_name' => 'required',
            'remarks' => 'required'
        ]);
        $prestigeCardHotel = PrestigeCardHotel::find($id);
        $prestigeCardHotel->hotel_name = $request->get('hotel_name');
        $prestigeCardHotel->country_id = $request->get('country_id');
        $prestigeCardHotel->remarks = $request->get('remarks');
        $prestigeCardHotel->mode = $request->get('mode');
        $prestigeCardHotel->save();

        if ($request->hasFile('input_img')) {
            if($request->file('input_img')->isValid()) {
                try {
                    $file = $request->file('input_img');
                    $name = time() . '.' . $file->getClientOriginalExtension();
                    $img = Image::make($request->file('input_img'));
                    $img->resize(250, 250);
                    $img->save(public_path("/uploads/" . $name));
                } catch (Illuminate\Filesystem\FileNotFoundException $e) {
                    echo $e->getMessage();
                }
            } 
        }

        return redirect('/prestige-card-hotels')->with('success', 'Hotel has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
