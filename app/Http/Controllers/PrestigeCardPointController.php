<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PrestigeCardPoint;

class PrestigeCardPointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($hotel_id)
    {
        $prestigeCardPoints = PrestigeCardPoint::where('hotel_id',$hotel_id)->get();
        return view('prestige_card_points.index', compact('prestigeCardPoints'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($hotel_id)
    {
        $prestigeCardPoint = new PrestigeCardPoint();
        $prestigeCardPoint->hotel_id = $hotel_id;
        return view('prestige_card_points.create', compact('prestigeCardPoint'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'hotel_id' => 'required|integer',
            'room_type' => 'required',
            'remarks' => 'required'
        ]);
        $prestigeCardPoint = new PrestigeCardPoint([
            'hotel_id' => $request->get('hotel_id'),
            'room_type' => $request->get('room_type'),
            'points' => 0,
            'low_season_points' => $request->get('low_season_points'),
            'mid_season_points' => $request->get('mid_season_points'),
            'high_season_points' => $request->get('high_season_points'),
            'peak_season_points' => $request->get('peak_season_points'),
            'upgrade_points' => $request->get('upgrade_points'),
            'remarks' => $request->get('remarks')
        ]);
        $prestigeCardPoint->save();
        return redirect('/prestige-card-points/' . $request->get('hotel_id'))->with('success', 'Points entry has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prestigeCardPoint = PrestigeCardPoint::find($id);
        return view('prestige_card_points.edit', compact('prestigeCardPoint'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'hotel_id' => 'required|integer',
            'room_type' => 'required',
            'remarks' => 'required'
        ]);
        $prestigeCardPoint = PrestigeCardPoint::find($id);
        $prestigeCardPoint->hotel_id = $request->get('hotel_id');
        $prestigeCardPoint->room_type = $request->get('room_type');
        $prestigeCardPoint->low_season_points = $request->get('low_season_points');
        $prestigeCardPoint->mid_season_points = $request->get('mid_season_points');
        $prestigeCardPoint->high_season_points = $request->get('high_season_points');
        $prestigeCardPoint->peak_season_points = $request->get('peak_season_points');
        $prestigeCardPoint->upgrade_points = $request->get('upgrade_points');
        $prestigeCardPoint->remarks = $request->get('remarks');
        $prestigeCardPoint->save();
        return redirect('/prestige-card-points/' . $request->get('hotel_id'))->with('success', 'Points entry has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}