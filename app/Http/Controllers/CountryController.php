<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::all();
        return view('countries.index', compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $country = new Country();
        return view('countries.create', compact('country'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'country_name' => 'required',
            'country_code' => 'required'
        ]);
        $country = new Country([
            'country_name' => $request->get('country_name'),
            'country_code' => $request->get('country_name'),
            'low_season' => $request->get('low_season'),
            'mid_season' => $request->get('mid_season'),
            'high_season' => $request->get('high_season'),
            'peak_season' => $request->get('peak_season')
        ]);
        $country->save();
        return redirect('/countries')->with('success', 'Country has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::find($id);
        return view('countries.edit', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'country_name' => 'required',
            'country_code' => 'required'
        ]);
        $country = Country::find($id);
        $country->country_name = $request->get('country_name');
        $country->country_code = $request->get('country_code');
        $country->low_season = $request->get('low_season');
        $country->mid_season = $request->get('mid_season');
        $country->high_season = $request->get('high_season');
        $country->peak_season = $request->get('peak_season');
        $country->save();
        return redirect('/countries')->with('success', 'Country has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
