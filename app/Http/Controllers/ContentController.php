<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContentType;
use App\Content;
use App\Tools\MemberRegistration;
use Illuminate\Support\Facades\Gate;
use Image;

class ContentController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function page($slug)
    {
        $page = Content::where('slug',$slug)->get();
        if(count($page) > 0) {
            $page = $page[0];
            return view('pages.view', compact('page'));
        } else {
            return view('pages.404');
        }
    }

    public function index()
    {
        if (Gate::allows('admin-only', auth()->user())) {
            $contents = Content::all();
            return view('contents.index', compact('contents'));
        }
        return view('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $content = new Content();
        $items = ContentType::all();
        return view('contents.create', compact('content','items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'title' => 'required',
        'intro' => 'required',
        'content_type_id' => 'required|integer',
        'intro' => 'required',
        'body' => 'required'
        ]);
        $slug = strtolower(str_replace(" ", "-",$request->get('title')));

        if ($request->hasFile('filename')) {
            if($request->file('filename')->isValid()) {
                try {
                    $file = $request->file('filename');
                    $name = time() . '.' . $file->getClientOriginalExtension();
                    $img = Image::make($request->file('filename'));
                    $img->save(public_path("/uploads/" . $name));
                } catch (Illuminate\Filesystem\FileNotFoundException $e) {
                    echo $e->getMessage();
                }
            } 
        }

        $content = new Content([
            'title' => $request->get('title'),
            'user_id' => auth()->user()->id,
            'slug' => $slug,
            'filename' => $name,
            'content_type_id' => $request->get('content_type_id'),
            'description' => $request->get('description'),
            'caption' => $request->get('caption'),
            'intro' => $request->get('intro'),
            'created_by' => 'samwaithaka',
            'body' => $request->get('body')
        ]);

        $content->save();
        return redirect('/contents')->with('success', 'Content type has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $content = Content::find($id);
        $items = ContentType::all();
        return view('contents.edit', compact('content','items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'intro' => 'required',
            'content_type_id' => 'required|integer',
            'intro' => 'required',
            'body' => 'required'
        ]);
        if ($request->hasFile('filename')) {
            if($request->file('filename')->isValid()) {
                try {
                    $file = $request->file('filename');
                    $name = time() . '.' . $file->getClientOriginalExtension();
                    $img = Image::make($request->file('filename'));
                    $img->save(public_path("/uploads/" . $name));
                } catch (Illuminate\Filesystem\FileNotFoundException $e) {
                    echo $e->getMessage();
                }
            } 
        }

        $content = Content::find($id);
        $content->title = $request->get('title');
        $content->user_id = auth()->user()->id;
        $content->content_type_id = $request->get('content_type_id');
        $content->description = $request->get('description');
        $content->caption = $request->get('caption');
        $content->intro = $request->get('intro');
        if(isset($name))
            $content->filename = $name;
        $content->body = $request->get('body');
        $content->save();
        return redirect('/contents')->with('success', 'Content type has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
