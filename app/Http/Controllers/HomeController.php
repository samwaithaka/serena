<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $suggestions = DB::table('suggestions')
        ->join('suggestion_types','suggestions.type_id','suggestion_types.id')
        ->where('suggestion_types.suggestion_type', 'like', '%slide%')
        ->orderByRaw('suggestions.created_at DESC')
        ->get();

        $offers = DB::table('suggestions')
        ->join('suggestion_types','suggestions.type_id','suggestion_types.id')
        ->where('suggestion_types.suggestion_type', 'like', '%offer%')
        ->orderByRaw('suggestions.created_at DESC')
        ->get();

        if(count($suggestions) > 0) {
            return view('index', compact('suggestions','offers'));
        } else {
            return view('pages.404');
        }
        //return view('home');
    }

    /**
  * Show the application private resources.
  *
  * @return \Illuminate\Http\Response
  */
   public function private()
   {
       if (Gate::allows('admin-only', auth()->user())) {
           return view('private');
        } 
        return "You are not admin!";
   }
}
