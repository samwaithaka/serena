<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
  * Show the application private resources.
  *
  * @return \Illuminate\Http\Response
  */
   public function private()
   {
       if (Gate::allows('admin-only', auth()->user())) {
           return view('private');
        } 
        return "You are not admin!";
   }
}
