<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Member;
use App\Country;
use App\Tools\MemberRegistration;

class MemberController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Member::all();
        return view('members.index', compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function enrol() {
        $member = new Member();
        $countries = Country::all();
        return view('members.enrol', compact('member','countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkPoints() {
        if(!auth()->user()) {
            return view('auth.login');
        }
        $member = MemberController::findByEmail(auth()->user()->email);
        if(count($member) > 0) {
            $args['CardNumber'] = $member[0]->membership_number;
            $args['EmailAddress'] = $member[0]->email_address;
            MemberRegistration::checkPoints($args);
        }
    }

    public static function showPoints($args) {
       return view('members.points', compact('args'));
    }

    public function create() {
        $member = new Member();
        $countries = Country::all();
        return view('members.create', compact('member','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $member = MemberController::findByEmail($request->get('email_address'));        
        if(count($member) > 0) {
            MemberController::update($request, $member[0]->id);
            MemberRegistration::syncWithBackend($request->all(), 3);
        } else {
            $request->validate([
                'cnic' => 'required',
                'name' => 'required',
                'password' => 'required',
                'date_of_birth' => 'required',
                'marital_status' => 'required',
                'gender' => 'required',
                'residence_address' => 'required',
                'residence_city' => 'required',
                'residence_province' => 'required',
                'residence_country' => 'required',
                'mobile_number' => 'required',
                'email_address' => 'required',
                'preferred_number' => 'required',
                'company_name' => 'required'
                //'status' => 'required'
            ]);
            $firstName = substr($request->get('name'), 0, stripos($request->get('name'), " "));
            $lastName = substr($request->get('name'), strripos($request->get('name'), " "), strlen($request->get('name'))); 
            $member = new Member([
                'cnic' => $request->get('cnic'),
                'name' => $request->get('name'),
                'first_name' => $firstName,
                'last_name' => $lastName,
                'password' => Hash::make($request->get('password')),
                'date_of_birth' => $request->get('date_of_birth'),
                'marital_status' => $request->get('marital_status'),
                'gender' => $request->get('gender'),
                'residence_address' => $request->get('residence_address'),
                'residence_city' => $request->get('residence_city'),
                'residence_province' => $request->get('residence_province'),
                'residence_country' => $request->get('residence_country'),
                'office_name' => $request->get('office_name'),
                'company_name' => $request->get('company_name'),
                'designation' => $request->get('designation'),
                'office_address' => $request->get('office_address'),
                'office_city' => $request->get('office_city'),
                'office_province' => $request->get('office_province'),
                'office_country' => $request->get('office_country'),
                'home_phone' => $request->get('home_phone'),
                'mobile_number' => $request->get('mobile_number'),
                'office_phone' => $request->get('office_phone'),
                'fax' => $request->get('fax'),
                'email_address' => $request->get('email_address'),
                'mailing_address' => $request->get('residence_address'),
                'preferred_number' => $request->get('preferred_number'),
                'membership_number' => $request->get('membership_number'),
                'status' => $request->get('status')
            ]);
            User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email_address'),
            'password' => Hash::make($request->get('password')),
            ]);
            $member->save();
            MemberRegistration::syncWithBackend($request->all(), 1);
        }
        return redirect('/members')->with('success', 'Member type has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::find($id);
        $countries = Country::all();
        return view('members.edit', compact('member','items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
            'cnic' => 'required',
            'name' => 'required',
            'password' => 'required',
            'date_of_birth' => 'required',
            'marital_status' => 'required',
            'gender' => 'required',
            'residence_address' => 'required',
            'residence_city' => 'required',
            'residence_province' => 'required',
            'residence_country' => 'required',
            'mobile_number' => 'required',
            'email_address' => 'required',
            'preferred_number' => 'required'
            //'membership_number' => 'required',
            //'status' => 'required'
        ]);
        $firstName = substr($request->get('name'), 0, stripos($request->get('name'), " "));
        $lastName = substr($request->get('name'), strripos($request->get('name'), " "), strlen($request->get('name'))); 
        $member = Member::find($id);
        $member->cnic = $request->get('cnic');
        $member->company_name = $request->get('company_name');
        $member->name = $request->get('name');
        $member->first_name = $firstName;
        $member->last_name = $lastName;
        $member->password = Hash::make($request->get('password'));
        $member->date_of_birth = $request->get('date_of_birth');
        $member->marital_status = $request->get('marital_status');
        $member->gender = $request->get('gender');
        $member->residence_address = $request->get('residence_address');
        $member->residence_city = $request->get('residence_city');
        $member->residence_province = $request->get('residence_province');
        $member->residence_country = $request->get('residence_country');
        $member->office_name = $request->get('office_name');
        $member->designation = $request->get('designation');
        $member->office_address = $request->get('office_address');
        $member->office_city = $request->get('office_city');
        $member->office_province = $request->get('office_province');
        $member->office_country = $request->get('office_country');
        $member->home_phone = $request->get('home_phone');
        $member->mobile_number = $request->get('mobile_number');
        $member->office_phone = $request->get('office_phone');
        $member->fax = $request->get('fax');
        $member->email_address = $request->get('email_address');
        $member->mailing_address = $request->get('mailing_address');
        $member->preferred_number = $request->get('preferred_number');
        $member->membership_number = $request->get('membership_number');
        $member->status = $request->get('status');
        $member->save();
        return redirect('/members')->with('success', 'Member type has been updated');
    }

    public function findByEmail($emailAddress)
    {
        $member = Member::where('email_address',$emailAddress)->get();
        return $member;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function updateFromBackend($args) {
        echo 123;
        return redirect('/members')->with('success', 'Member type has been updated');
        return redirect('/confirm')->with('error', 'Member not enrolled, an error occured!');
        /*$emailAddress = $args['EmailAddress'];
        $member = Member::where('email_address',$emailAddress)
              ->get();
        if(count($member) > 0) {
            $member = Member::find($member[0]->id);
            $member->membership_number = $args['MembershipNumber'];
            $member->status = $args['Status'];
            $member->save();
            return redirect('/confirm')->with('success', 'Member type has been successfully enrolled');
        }
        return redirect('/confirm')->with('error', 'Member not enrolled, an error occured!');*/
    }
}