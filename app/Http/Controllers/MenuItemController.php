<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\MenuItem;

class MenuItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($menu_id)
    {
        $menuItems = MenuItem::where('menu_id',$menu_id)->get();
        return view('menu_items.index', compact('menuItems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($menu_id)
    {
        $menuItem = new MenuItem();
        $menuItem->menu_id = $menu_id;
        return view('menu_items.create', compact('menuItem'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'menu_id' => 'required|integer',
        'title' => 'required',
        'uri' => 'required'
        ]);
        $menuItem = new MenuItem([
        'menu_id' => $request->get('menu_id'),
        'title' => $request->get('title'),
        'uri' => $request->get('uri')
        ]);
        $menuItem->save();
        $menuId = $request->get('menu_id');
        return redirect('/menu-items/' . $menuId)->with('success', 'Menu item has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menuItem = MenuItem::find($id);
        return view('menu_items.edit', compact('menuItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
        'menu_id' => 'required|integer',
        'title' => 'required',
        'uri' => ''
        ]);
        $menuItem = MenuItem::find($id);
        $menuItem->menu_id = $request->get('menu_id');
        $menuItem->title = $request->get('title');
        $menuItem->uri = $request->get('uri');
        $menuItem->save();
        $menuId = $request->get('menu_id');
        return redirect('/menu-items/' . $menuId)->with('success', 'Menu item has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
