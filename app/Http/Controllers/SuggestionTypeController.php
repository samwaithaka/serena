<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SuggestionType;

class SuggestionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suggestionTypes = SuggestionType::all();
        return view('suggestion_types.index', compact('suggestionTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suggestionType = new SuggestionType();
        return view('suggestion_types.create', compact('suggestionType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'suggestion_type' => 'required',
        'description' => 'required'
        ]);
        $suggestionType = new SuggestionType([
        'suggestion_type' => $request->get('suggestion_type'),
        'description' => $request->get('description')
        ]);
        $suggestionType->save();
        return redirect('/suggestion-types')->with('success', 'Content type has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $suggestionType = SuggestionType::find($id);
        return view('suggestion_types.edit', compact('suggestionType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'suggestion_type' => 'required',
            'description' => 'required'
        ]);
        $suggestionType = SuggestionType::find($id);
        $suggestionType->suggestion_type = $request->get('suggestion_type');
        $suggestionType->description = $request->get('description');
        $suggestionType->save();
        return redirect('/suggestion-types')->with('success', 'Content type has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
