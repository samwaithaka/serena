<?php

namespace App\Http\Controllers;

use App\PrestigeCardMealPoint;
use Illuminate\Http\Request;

class PrestigeCardMealPointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($hotel_id)
    {
        $prestigeCardMealPoints = PrestigeCardMealPoint::where('hotel_id',$hotel_id)->get();
        return view('prestige_card_meal_points.index', compact('prestigeCardMealPoints'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($hotel_id)
    {
        $prestigeCardMealPoint = new PrestigeCardMealPoint();
        $prestigeCardMealPoint->hotel_id = $hotel_id;
        return view('prestige_card_meal_points.create', compact('prestigeCardMealPoint'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'hotel_id' => 'required|integer',
            'breakfast_points' => 'required',
            'lunch_points' => 'required',
            'dinner_points' => 'required'
        ]);
        $prestigeCardMealPoint = new PrestigeCardMealPoint([
            'hotel_id' => $request->get('hotel_id'),
            'breakfast_points' => $request->get('breakfast_points'),
            'lunch_points' => $request->get('lunch_points'),
            'dinner_points' => $request->get('dinner_points')
        ]);
        $prestigeCardMealPoint->save();
        return redirect('/prestige-card-meal-points/' . $request->get('hotel_id'))->with('success', 'Points entry has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PrestigeCardMealPoint  $prestigeCardMealPoint
     * @return \Illuminate\Http\Response
     */
    public function show(PrestigeCardMealPoint $prestigeCardMealPoint)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PrestigeCardMealPoint  $prestigeCardMealPoint
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prestigeCardMealPoint = PrestigeCardMealPoint::find($id);
        return view('prestige_card_meal_points.edit', compact('prestigeCardMealPoint'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PrestigeCardMealPoint  $prestigeCardMealPoint
     * @return \Illuminate\Http\Response
     */
public function update(Request $request, $id)
    {
        $request->validate([
            'hotel_id' => 'required|integer',
            'breakfast_points' => 'required',
            'lunch_points' => 'required',
            'dinner_points' => 'required'
        ]);
        $prestigeCardMealPoint = PrestigeCardMealPoint::find($id);
        $prestigeCardMealPoint->hotel_id = $request->get('hotel_id');
        $prestigeCardMealPoint->breakfast_points = $request->get('breakfast_points');
        $prestigeCardMealPoint->lunch_points = $request->get('lunch_points');
        $prestigeCardMealPoint->dinner_points = $request->get('dinner_points');
        $prestigeCardMealPoint->save();
        return redirect('/prestige-card-meal-points/' . $request->get('hotel_id'))->with('success', 'Points entry has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PrestigeCardMealPoint  $prestigeCardMealPoint
     * @return \Illuminate\Http\Response
     */
    public function destroy(PrestigeCardMealPoint $prestigeCardMealPoint)
    {
        //
    }
}
