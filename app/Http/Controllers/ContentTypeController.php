<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContentType;

class ContentTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contentTypes = ContentType::all();
        return view('content_types.index', compact('contentTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $contentType = new ContentType();
        return view('content_types.create', compact('contentType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'content_type_text' => 'required'
        ]);
        $contentType = new ContentType([
        'content_type_text' => $request->get('content_type_text')
        ]);
        $contentType->save();
        return redirect('/content-types')->with('success', 'Content type has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contentType = ContentType::find($id);
        return view('content_types.edit', compact('contentType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
        'content_type_text' => 'required'
        ]);
        $contentType = ContentType::find($id);
        $contentType->content_type_text = $request->get('content_type_text');
        $contentType->save();
        return redirect('/content-types')->with('success', 'Content type has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
