<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\SuggestionType;
use App\Suggestion;
use Image;

class SuggestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type_id)
    {
        $suggestions = Suggestion::where('type_id',$type_id)->get();
        return view('suggestions.index', compact('suggestions'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function display($key)
    {
        $suggestions = DB::table('suggestions')
        ->join('suggestion_types','suggestions.type_id','suggestion_types.id')
        ->where('suggestion_types.suggestion_type', 'like', '%' . $key . '%')
        ->orderByRaw('suggestions.created_at DESC')
        ->get();
        if(count($suggestions) > 0) {
            return view('suggestions.display', compact('suggestions'));
        } else {
            return view('pages.404');
        }
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view($slug)
    {
        $suggestion = Suggestion::where('slug',$slug)->get();
        //echo "<pre>"; print_r($suggestion);exit;
        if(count($suggestion) > 0) {
            $suggestion = $suggestion[0];
            return view('suggestions.view', compact('suggestion'));
        } else {
            return view('pages.404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type_id)
    {
        $suggestion = new Suggestion();
        $suggestion->type_id = $type_id;
        return view('suggestions.create', compact('suggestion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'type_id' => 'required|integer',
        'title' => 'required',
        'description' => 'required',
        'body' => 'required'
        ]);

        if ($request->hasFile('poster')) {
            if($request->file('poster')->isValid()) {
                try {
                    $file = $request->file('poster');
                    $name = time() . '.' . $file->getClientOriginalExtension();
                    $img = Image::make($request->file('poster'));
                    if($request->get('link') == null) {
                        $img->resize(700, 700);
                    }
                    $img->save(public_path("/uploads/" . $name));
                } catch (Illuminate\Filesystem\FileNotFoundException $e) {
                    echo $e->getMessage();
                }
            } 
        }

        $slug = strtolower(str_replace(" ", "-",$request->get('title')));

        $link = $request->get('link');
        if(!$link)
            $link = $slug;

        $suggestion = new Suggestion([
        'type_id' => $request->get('type_id'),
        'title' => $request->get('title'),
        'description' => $request->get('description'),
        'file_name' => $name,
        'slug' => $slug,
        'link' => $link,
        'body' => $request->get('body')
        ]);
        $suggestion->save();
        $typeId = $request->get('type_id');
        return redirect('/suggestions/' . $typeId)->with('success', 'Suggestion has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $suggestion = Suggestion::find($id);
        return view('suggestions.edit', compact('suggestion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
        'type_id' => 'required|integer',
        'title' => 'required',
        'description' => 'required',
        'body' => 'required'
        ]);
        if ($request->hasFile('poster')) {
            if($request->file('poster')->isValid()) {
                try {
                    $file = $request->file('poster');
                    $name = time() . '.' . $file->getClientOriginalExtension();
                    $img = Image::make($request->file('poster'));
                    if($request->get('link') == null) {
                        $img->resize(700, 700);
                    }
                    $img->save(public_path("/uploads/" . $name));
                } catch (Illuminate\Filesystem\FileNotFoundException $e) {
                    echo $e->getMessage();
                }
            } 
        }

        $slug = strtolower(str_replace(" ", "-",$request->get('title')));
        $link = $request->get('link');
        $suggestion = Suggestion::find($id);
        $suggestion->type_id = $request->get('type_id');
        $suggestion->title = $request->get('title');
        $suggestion->description = $request->get('description');
        $suggestion->body = $request->get('body');
        $suggestion->slug = $slug;
        if(!$link)
            $link = $slug;
        $suggestion->link = $link;
        if(isset($name))
            $suggestion->file_name = $name;
        $suggestion->save();
        $typeId = $request->get('type_id');
        return redirect('/suggestions/' . $typeId)->with('success', 'Suggestion has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
