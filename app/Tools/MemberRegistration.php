<?php

namespace App\Tools;

use App\Member;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Tools\Mailer;

use App\Http\Requests;

class MemberRegistration {
	public static function syncWithBackend($args, $requestType) {
        $firstName = trim(substr($args['name'], 0, stripos($args['name'], " ")));
        $lastName = trim(substr($args['name'], strripos($args['name'], " "), strlen($args['name'])));
        $returnURL = env('APP_URL') . "/api/registration";
        //$returnURL = "http://serenaprestigeclub.com/tests/received.php";
        $APIURL = config("app.api_url");
        $requestType = 1;
        $postdata = http_build_query(
        array(
            'RequestType'=> $requestType,
            'ReturnURL' => $returnURL,
            'FirstName' =>$firstName,
            'MiddleName' =>"",
            'LastName' =>$lastName,
            'NameOnCard' => $args['name'],
            'CNIC'=>$args['cnic'],
            'Designation'=> "",
            'OfficeName'=>$args['company_name'],
            'MaritalStatus' => $args['marital_status'],
            'Gender'=>$args['gender'],
            'MailingAddress'=>$args['residence_address'],
            'ResidenceCountryID'=>$args['residence_country'],
            'OfficeCountryID'=> $args['residence_country'],
            'EmailAddress'=>$args['email_address'],
            'Password'=> Hash::make($args['password']),
            'PreferredNumber' => $args['preferred_number'],
            'ResidenceAddress' => $args['residence_address'],
            'ResidenceCity' => $args['residence_city'],
            'ResidenceProvince'=> $args['residence_province'],
            'OfficeAddress'=> "",
            'OfficeCity'=> "",
            'OfficeProvince'=> "",
            'HomePhone'=> "",
            'OfficePhone'=> "",
            'Fax'=> "",
            'TemporaryCardNumber' => "",
            'Mobile' => $args['mobile_number'],
            'DateOfBirth' =>$args['date_of_birth']));
        MemberRegistration::postToAPI($APIURL, $postdata);
	}

	public static function checkPoints($args) {
        $returnURL = env('APP_URL') . "/api/points";
        $APIURL = config('app.api_url');
        $postdata = http_build_query(
            array(
                'RequestType'=> 2,
                'ReturnURL' => $returnURL,
                'EmailAddress' => $args['CardNumber'],
                'Password' => $args['CardNumber']
            )
        );
	    MemberRegistration::postToAPI($APIURL, $postdata);
	}

	public static function updateOnLocal($args) {
        $emailAddress = $args['EmailAddress'];
        $password = $args['Password'];
        $member = Member::where('email_address',$emailAddress)
              //->where('password','like','%' . $password . '%')
              ->get();
        if(count($member) > 0) {
            $member = Member::find($member[0]->id);
            $member->membership_number = $args['MembershipNumber'];
            $member->status = $args['Status'];
            $member->save();
            $fromName = "Prestige Club Admin"; 
            $fromEmail = "admin@serenaprestigeclub.com"; 
            $subject = "Registration Successful"; 
            $body = MemberRegistration::getRegistrationEmailMessage($member->first_name, 
                $member->email_address, $member->membership_number);
            Mailer::sendEmail($member->email_address, 
                $member->name, 
                $fromName, 
                $fromEmail,
                $subject,
                nl2br($body));
            return true;
        }
        return false;
	}

	public static function syncWithLocal($args) {
		$member = new Member([
            'cnic' => $args['CNIC'],
            'name' => $args['NameOnCard'],
            'password' => $args['CardNumber'],
            'date_of_birth' => $args['DateOfBirth'],
            'marital_status' => $args['MaritalStatus'],
            'gender' => $args['Gender'],
            'recidence_address' => $args['RecidenceAddress'],
            'recidence_city' => $args['RecidenceCity'],
            'recidence_province' => $args['RecidenceProvince'],
            'recidence_country' => $args['RecidenceCountryID'],
            'office_name' => $args['OfficeName'],
            'designation' => $args['Designation'],
            'office_address' => $args['OfficeAddress'],
            'office_city' => $args['OfficeCity'],
            'office_province' => $args['OfficeProvince'],
            'office_country' => $args['OfficeCountry'],
            'home_phone' => $args['HomePhone'],
            'mobile_number' => $args['Mobile'],
            'office_phone' => $args['OfficePhone'],
            'fax' => $args['Fax'],
            'email_address' => $args['EmailAddress'],
            'mailing_address' => $args['Mailing_Address'],
            'preferred_number' => $args['PreferredNumber'],
            'membership_number' => $args['MembershipNumber'],
            'status' => $args['Status']
        ]);
        $member->save();
	}

	public static function postToAPI($APIURL, $postdata) {
		$ch = curl_init($APIURL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_POST,1);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
	    curl_setopt($ch, CURLOPT_HEADER,0);  // DO NOT RETURN HTTP HEADERS
	    if(curl_exec($ch)) {
            echo "Wait...";
        } else {
            echo 'Curl error: ' . curl_error($ch);
        }
	    curl_close($ch);
	    exit;
	}

    public static function getRegistrationEmailMessage($firstName, $login, $CardNumber) {
        $message = "Dear {$firstName},

Thank you for subscribing for the Serena Prestige Club card! You can now earn Serena points on various products and services offered by Serena Hotels.

Your membership number is {$CardNumber}.

Please login using the following url: http://serenaprestigeclub.com.
Your login username is: {$login}

When transacting at any of our Serena Properties, remember to always present your card, which will either be swiped or keyed in allowing any transaction to be captured by the card. After accruing points, you can Redeem (Burn) your points at any of the Serena Properties.

For more information, you may visit our website: www.serenaprestigeclub.com or contact us on the following below.

Africa Office
Manager Serena Prestige Club,
Tel: +254-20-2842000
Fax: +254-20-2718100
Email: prestigeclub@serena.co.ke";
        return $message;
    }
}

?>