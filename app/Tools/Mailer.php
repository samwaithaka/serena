<?php

namespace App\Tools;

use App\Member;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;

class Mailer {
	public static function sendEmail($toEmail, $toName, $fromName, $fromEmail, $subject, $body) {
      $data['subject'] = $subject;
      $data['body'] = $body;

      Mail::send('mail', $data, function($message) use ($toEmail, $toName, $fromName, $fromEmail, $subject) {
         $message->to($toEmail, $toName)->subject($subject);
         $message->from($fromEmail, $fromName);
      });
    }
}

?>