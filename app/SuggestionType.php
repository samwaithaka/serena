<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuggestionType extends Model
{
    protected $fillable = [
        'suggestion_type',
        'description'
  ];
}
