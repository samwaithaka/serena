<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $fillable = [
    'content_type_id',
    'title',
    'slug',
    'description',
    'caption',
    'filename',
    'intro',
    'body',
    'created_by',
    'user_id'
  ];
}