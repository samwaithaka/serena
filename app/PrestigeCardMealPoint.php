<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrestigeCardMealPoint extends Model
{
    protected $fillable = [
        'hotel_id',
        'breakfast_points',
        'lunch_points',
        'dinner_points'
    ];
}
