<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrestigeCardHotel extends Model
{
    protected $fillable = [
        'hotel_name',
        'country_id',
        'mode',
        'remarks'
    ];
}
