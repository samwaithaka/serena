<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrestigeCardPoint extends Model
{
    protected $fillable = [
        'hotel_id',
        'room_type',
        'points',
        'low_season_points',
        'mid_season_points',
        'high_season_points',
        'peak_season_points',
        'upgrade_points',
        'remarks'
    ];
}
