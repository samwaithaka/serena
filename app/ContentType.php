<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentType extends Model
{
    protected $fillable = [
    'content_type_text'
  ];
}
