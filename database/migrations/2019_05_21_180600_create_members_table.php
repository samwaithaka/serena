<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cnic');
            $table->string('name');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('password');
            $table->date('date_of_birth');
            $table->string('marital_status');
            $table->string('gender');
            $table->string('recidence_address');
            $table->string('recidence_city');
            $table->string('recidence_province');
            $table->integer('recidence_country');
            $table->string('office_name');
            $table->string('designation');
            $table->string('office_address');
            $table->string('office_city');
            $table->string('office_province');
            $table->integer('office_country');
            $table->string('home_phone');
            $table->string('mobile_number');
            $table->string('office_phone');
            $table->string('fax');
            $table->string('email_address');
            $table->string('mailing_address');
            $table->string('preferred_number');
            $table->string('membership_number');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
