<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeasonsToPrestigeCardPoints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prestige_card_points', function (Blueprint $table) {
            $table->integer('low_season_points');
            $table->integer('mid_season_points');
            $table->integer('high_season_points');
            $table->integer('peak_season_points');
            $table->integer('upgrade_points');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prestige_card_points', function (Blueprint $table) {
            $table->dropColumn('low_season_points');
            $table->dropColumn('mid_season_points');
            $table->dropColumn('high_season_points');
            $table->dropColumn('peak_season_points');
            $table->dropColumn('upgrade_points');
        });
    }
}