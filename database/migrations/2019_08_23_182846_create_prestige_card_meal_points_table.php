<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestigeCardMealPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestige_card_meal_points', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_id');
            $table->string('breakfast_points');
            $table->string('lunch_points');
            $table->string('dinner_points');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestige_card_meal_points');
    }
}
