-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: spc_db
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_types`
--

DROP TABLE IF EXISTS `content_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_type_text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_types`
--

LOCK TABLES `content_types` WRITE;
/*!40000 ALTER TABLE `content_types` DISABLE KEYS */;
INSERT INTO `content_types` VALUES (1,'General2','2019-05-22 15:06:06','2019-05-22 15:22:13'),(2,'Blogs','2019-05-22 15:11:44','2019-05-22 15:11:44'),(3,'Other Posts','2019-05-22 15:24:49','2019-05-22 15:24:49');
/*!40000 ALTER TABLE `content_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_type_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `intro` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` varchar(4550) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents`
--

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
INSERT INTO `contents` VALUES (1,2,'Item22','item-three','Intro33','<p>Body</p>','samwaithaka','2019-05-23 18:48:31','2019-06-04 14:03:06','Intro33',1),(2,1,'About Us','about-us','Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<h3>Section 1.10.32 of de Finibus Bonorum et Malorum, written by Cicero in 45 BC</h3>\r\n<p>\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam</p>','samwaithaka','2019-06-13 13:38:38','2019-06-13 13:38:38','Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',1);
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Pakistan',92,'2019-05-28 18:59:34','2019-05-28 18:59:34'),(2,'Afghanistan',93,'2019-05-28 18:59:34','2019-05-28 18:59:34'),(3,'Tajikistan',992,'2019-05-28 18:59:34','2019-05-28 18:59:34'),(4,'Kenya',254,'2019-05-28 18:59:34','2019-05-28 18:59:34'),(5,'Tanzania',255,'2019-05-28 18:59:34','2019-05-28 18:59:34'),(6,'Mozambique',258,'2019-05-28 18:59:34','2019-05-28 18:59:34'),(7,'Uganda',256,'2019-05-28 18:59:34','2019-05-28 18:59:34'),(8,'Rwanda',250,'2019-05-28 18:59:34','2019-05-28 18:59:34'),(9,'Kazakhstan',76,'2019-05-28 18:59:34','2019-05-28 18:59:34'),(10,'India',91,'2019-05-28 18:59:34','2019-05-28 18:59:34'),(11,'Zanzibar',255,'2019-05-28 18:59:34','2019-05-28 18:59:34'),(12,'Burundi',257,'2019-05-28 18:59:34','2019-05-28 18:59:34'),(13,'Kyrgyzstan',996,'2019-05-28 18:59:34','2019-05-28 18:59:34');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cnic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `marital_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `residence_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `residence_city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `residence_province` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `residence_country` int(11) NOT NULL,
  `office_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `office_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `office_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `office_province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `office_country` int(11) DEFAULT NULL,
  `home_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `office_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mailing_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preferred_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `membership_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members`
--

LOCK TABLES `members` WRITE;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
INSERT INTO `members` VALUES (1,'20762033','Samuel Waithaka','Samuel',' Waithaka','$2y$10$Tk4o9upsLo1rBNGFPxacMeVnxUK9aPw4Jo8ald9n3j24nAytNnUo.','2019-06-24','Married','Male','Kikuyu Gitaru','Kiambu','Kiambu',254,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0720317929',NULL,NULL,'samwaithaka@gmail.com',NULL,'0720317929',NULL,NULL,'2019-05-28 16:10:59','2019-06-24 16:14:20','BrighterDayMedia Ltd'),(2,'20762033','Samuel Waithaka','Samuel',' Waithaka','password','2019-05-01','Married','Male','Gitaru','Nairobi','Nairobi',254,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0720317929',NULL,NULL,'sam@nextramile.com',NULL,'0720317929','1103000010025767',NULL,'2019-05-28 16:12:17','2019-05-28 16:12:17',''),(3,'20762033','Mwaniki Wa Waithaka','Mwaniki',' Waithaka','$2y$10$rZ3PpsnPQGb6lfLLe6bK/.Y5FcKpLSfZ7ub02hpRPsuCJx3gkBBk2','2019-06-08','Married','Male','Kikuyu Gitaru','Kiambu','Kiambu',254,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0720317929',NULL,NULL,'sam2@nextramile.com','Kikuyu Gitaru','0720317929',NULL,NULL,'2019-06-08 13:49:09','2019-06-08 13:49:09','BrighterDayMedia Ltd'),(4,'20762033','Samuel Waithaka','Samuel',' Waithaka','$2y$10$HHrhRKDDznSsWPvEZz7BhePVkVMFHiOjcm0Gzjttmhv2M4ggbZsIm','2019-06-08','Married','Male','Kikuyu Gitaru','Kiambu','Kiambu',254,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0720317929',NULL,NULL,'sam7@nextramile.com','Kikuyu Gitaru','0720317929','1103000010025770',NULL,'2019-06-08 14:12:39','2019-06-08 14:12:39','BrighterDayMedia Ltd'),(5,'20762033','Samuel Waithaka','Samuel',' Waithaka','$2y$10$WlRj/5/.jYWt1GPU2P3Lbuv1GEDbzUqboX4X/jFFS2Tr.pvqPx3le','2019-06-16','Married','Male','Kikuyu Gitaru','Kiambu','Kiambu',254,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0720317929',NULL,NULL,'samwaithaka22@gmail.com',NULL,'0720317929',NULL,NULL,'2019-06-16 16:01:42','2019-06-16 16:03:53','BrighterDayMedia Ltd'),(6,'20762033','Samuel Waithaka','Samuel',' Waithaka','$2y$10$zR1nETVTPNa.oJTQ0W8vQeAW8G3VB66ULHPiwwzyRYRnGYJ0GOdSK','2019-06-16','Married','Male','Kikuyu Gitaru','Kiambu','Kiambu',254,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0720317929',NULL,NULL,'samwaithaka2233@gmail.com','Kikuyu Gitaru','0720317929',NULL,NULL,'2019-06-16 16:04:28','2019-06-16 16:04:28','BrighterDayMedia Ltd'),(7,'20762033','Samuel Waithaka','Samuel',' Waithaka','$2y$10$qGEzu1r0mgDW14jPr60kwuGz5BbO20RV9dCz293gJ.Og4zaYcHnU6','2019-06-17','Married','Male','Kikuyu Gitaru','Kiambu','Kiambu',254,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0720317929',NULL,NULL,'sam8@nextramile.com',NULL,'0720317929',NULL,NULL,'2019-06-17 00:56:54','2019-06-17 01:02:53','BrighterDayMedia Ltd'),(8,'20762033','Samuel Waithaka','Samuel',' Waithaka','$2y$10$0Fx/dvwYjqbUq3fSN1QAiuDOF96G6Ds50KXcNlNLwl50Bq9gkFLym','2019-06-17','Married','Male','Kikuyu Gitaru','Kiambu','Kiambu',254,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0720317929',NULL,NULL,'sam9@nextramile.com','Kikuyu Gitaru','0720317929','1103000010025773','E0','2019-06-17 14:18:10','2019-06-17 15:59:26','BrighterDayMedia Ltd');
/*!40000 ALTER TABLE `members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Home','en/home','2019-06-05 15:26:03','2019-06-05 15:42:06'),(2,1,'Reward Points','serena-hotels/points','2019-06-05 15:26:23','2019-06-24 14:58:18'),(3,1,'About Us','en/about-us','2019-06-05 15:37:19','2019-06-13 13:39:08'),(4,1,'Offers','display/offers','2019-07-16 18:48:53','2019-07-16 18:48:53'),(5,1,'Experiences','display/experiences','2019-07-16 18:49:10','2019-07-16 18:49:10');
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'Main Menu','2019-06-05 15:25:48','2019-06-05 15:25:48');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_05_21_180446_create_menu_items_table',2),(4,'2019_05_21_180503_create_menus_table',2),(5,'2019_05_21_180514_create_content_types_table',2),(6,'2019_05_21_180522_create_contents_table',2),(7,'2019_05_21_180530_create_countries_table',2),(8,'2019_05_21_180600_create_members_table',2),(9,'2019_05_25_063437_add_description_to_contents',3),(10,'2019_05_29_181732_add_company_name_to_members',4),(11,'2019_05_29_183117_add_mailing_address_to_members',5),(12,'2019_06_01_140825_create_contacts_table',5),(13,'2019_06_01_141116_create_prestige_card_points_table',5),(14,'2019_06_01_141132_create_prestige_card_hotels_table',5),(15,'2019_06_02_174105_create_roles_table',6),(16,'2019_06_02_174255_create_role_users_table',7),(17,'2019_06_03_175453_add_user_id_to_contents',8),(18,'2019_06_03_182506_add_user_id_to_contents',9),(19,'2019_06_03_182525_add_is_admin_to_users',9),(20,'2019_06_15_141533_add_country_to_prestige_hotels',10),(21,'2019_07_15_183518_create_suggestions_table',11),(22,'2019_07_15_183539_create_suggestion_types_table',11),(23,'2019_07_16_203133_add_slug_to_suggestions',12);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prestige_card_hotels`
--

DROP TABLE IF EXISTS `prestige_card_hotels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prestige_card_hotels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prestige_card_hotels`
--

LOCK TABLES `prestige_card_hotels` WRITE;
/*!40000 ALTER TABLE `prestige_card_hotels` DISABLE KEYS */;
INSERT INTO `prestige_card_hotels` VALUES (1,'Serena Nairobi','Discouted','2019-06-01 14:17:05','2019-06-15 11:41:32',4),(2,'Serena Mombasa','Discouted','2019-06-01 14:22:32','2019-06-15 11:41:40',6),(3,'Serena','NA','2019-06-23 16:01:29','2019-06-23 16:07:20',1);
/*!40000 ALTER TABLE `prestige_card_hotels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prestige_card_points`
--

DROP TABLE IF EXISTS `prestige_card_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prestige_card_points` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `room_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `points` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prestige_card_points`
--

LOCK TABLES `prestige_card_points` WRITE;
/*!40000 ALTER TABLE `prestige_card_points` DISABLE KEYS */;
INSERT INTO `prestige_card_points` VALUES (1,1,'Standard Single','1400','NA','2019-06-01 14:34:02','2019-06-01 14:34:02'),(2,1,'Deluxe Room','1600','NA','2019-06-01 14:38:09','2019-06-01 14:38:09'),(3,1,'Superior Room','2000','NA','2019-06-01 14:39:28','2019-06-01 14:39:28'),(4,2,'Standard Single','1430','NA','2019-06-01 14:40:12','2019-06-01 14:40:12'),(5,2,'Deluxe Room','1625','NA','2019-06-01 14:40:29','2019-06-01 14:40:29');
/*!40000 ALTER TABLE `prestige_card_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suggestion_types`
--

DROP TABLE IF EXISTS `suggestion_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suggestion_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `suggestion_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suggestion_types`
--

LOCK TABLES `suggestion_types` WRITE;
/*!40000 ALTER TABLE `suggestion_types` DISABLE KEYS */;
INSERT INTO `suggestion_types` VALUES (1,'Offers','Offers','2019-07-16 16:12:51','2019-07-16 16:12:51'),(2,'Experiences','Experiences','2019-07-16 16:13:02','2019-07-16 16:13:02');
/*!40000 ALTER TABLE `suggestion_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suggestions`
--

DROP TABLE IF EXISTS `suggestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suggestions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suggestions`
--

LOCK TABLES `suggestions` WRITE;
/*!40000 ALTER TABLE `suggestions` DISABLE KEYS */;
INSERT INTO `suggestions` VALUES (1,1,'Lorem Ipsum Dolor Sit','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','1563304741.jpeg','2019-07-16 16:19:02','2019-07-16 17:38:27','lorem-ipsum-dolor-sit'),(2,1,'Lorem Ipsum Dolor Sit 2','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','1563307814.jpg','2019-07-16 17:10:14','2019-07-16 17:38:58','lorem-ipsum-dolor-sit-2'),(3,1,'Lorem Ipsum Dolor Sit 3','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','1563310141.jpg','2019-07-16 17:49:02','2019-07-16 17:49:02','lorem-ipsum-dolor-sit-3'),(4,1,'Lorem Ipsum Dolor Sit 4','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat','1563310240.jpg','2019-07-16 17:50:40','2019-07-16 17:50:40','lorem-ipsum-dolor-sit-4'),(5,2,'Experience 1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat','1563313264.jpg','2019-07-16 18:41:04','2019-07-16 18:41:04','experience-1');
/*!40000 ALTER TABLE `suggestions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Samuel Waithaka','samwaithaka@gmail.com',NULL,'$2y$10$dPEoeW31iL4i6FcDJeT4Tegzw.L4ta5.xzqyvkq8VR349x0I/eCJC',NULL,'2019-06-01 15:41:39','2019-06-01 15:41:39',1),(2,'Mwaniki Waithaka','sam2@nextramile.com',NULL,'$2y$10$ZBiT1I12gIEQzdwD0CJozOIrAVv8s/O0PSKz.4IoLN5dl2ceaxt7q',NULL,'2019-06-03 15:56:53','2019-06-03 15:56:53',0),(4,'Samuel Waithaka','sam@nextramile.com',NULL,'$2y$10$41.OQyMu7BMEESqJo6ZYbejFklKF0zElVdPlyR0EAsymcqwi6eQZ6',NULL,'2019-06-06 17:30:55','2019-06-06 17:30:55',0),(5,'Samuel Waithaka','sam3@nextramile.com',NULL,'$2y$10$hRRsqS48e2bANHbI8eEGZeE2IgkVueG4755GiK8P5BnsOEKmdIFaG',NULL,'2019-06-08 12:56:32','2019-06-08 12:56:32',0),(7,'Mwaniki Wa Waithaka','sam4@nextramile.com',NULL,'$2y$10$4LpCasI6gDusqtZXwSCs9OX.O2vpOXp0eDosiwPZEhGM2aHVfap4.',NULL,'2019-06-08 13:02:10','2019-06-08 13:02:10',0),(8,'Mwaniki Wa Waithaka','sam5@nextramile.com',NULL,'$2y$10$CJpGzv/8yNNzjvSkOwwimeSqJ21W4VSLDlEHBbQSht5Rpc4YWmVZO',NULL,'2019-06-08 13:02:51','2019-06-08 13:02:51',0),(9,'Samuel Waithaka','sam7@nextramile.com',NULL,'$2y$10$kxpSGfc5mkqZ2XYI1ipTiOh9unIHaMdnHyESyGMdMXKnlVTC7vfQO',NULL,'2019-06-08 14:12:39','2019-06-08 14:12:39',0),(10,'Samuel Waithaka','samwaithaka22@gmail.com',NULL,'$2y$10$6/9MoTp24NGkRnQ6a3gIsOHWtxsMZgI6TXTfGCj7CUvlaD/.IUvnG',NULL,'2019-06-16 16:01:42','2019-06-16 16:01:42',0),(11,'Samuel Waithaka','samwaithaka2233@gmail.com',NULL,'$2y$10$MyAQbbl/mNtwd79xzSjmHOfl13EJNbyF3lQWT8pmypGsvCCrNRKPG',NULL,'2019-06-16 16:04:28','2019-06-16 16:04:28',0),(12,'Samuel Waithaka','sam8@nextramile.com',NULL,'$2y$10$1fYcCgHrZQ2gvG8d790BausImDEpVqSdhGCq3ZrmRxpw91Bl8y.9e',NULL,'2019-06-17 00:56:54','2019-06-17 00:56:54',0),(14,'Samuel Waithaka','sam9@nextramile.com',NULL,'$2y$10$jUrI6UDLn85l4FEP5NTo/eccATrRcVv/VVgMVNBr6rPqzUQ/puGlK',NULL,'2019-06-17 14:18:10','2019-06-17 14:18:10',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-17  1:03:19
